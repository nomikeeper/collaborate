import {
    StyleSheet,
    Dimensions,
    Platform,
} from "react-native";

// ------ Import colors
import Colors from "../../Theme/colors";

const { width, height} = Dimensions.get('window');

export default StyleSheet.create({
    wrapper:{
        flex:1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: Colors.general.white,
    },
    comingSoon:{
        fontFamily: Platform.OS == "ios" ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:24,
        fontWeight: "300",
        color: Colors.stylish.tomato
    }
})