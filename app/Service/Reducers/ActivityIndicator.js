import Actions from "../Actions/ActivityIndicator";

const initialState = {
    isActive: false,
}

const ActivityIndicatorReducer = ( state = initialState, action) => {
    switch(action.type){
        case Actions.ACTIVATE: {
            return {
                ...state, 
                isActive: true 
            }
        }
        case Actions.DEACTIVATE: {
            return{
                ...state,
                isActive: false
            }
        }
        default: return state;
    }
}

export default ActivityIndicatorReducer;