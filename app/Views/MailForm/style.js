import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";
// Importing Colors
import Colors from "../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper:{
        flex: 1,
        backgroundColor: Colors.general.white,
    },
    titleWrapper:{

    },
    titleLabel:{
        fontFamily: 'Montserrat',
        fontSize: 20,
        color: Colors.stylish.tomato,
    },
    backButton:{
        left: 10,
        paddingHorizontal:10,
    },
    field:{
        paddingHorizontal: 20,
        marginVertical: 10,
    },
    labelWrapper:{
        alignItems: 'flex-start'
    },
    labelBorder:{
        borderBottomWidth: 2,
        borderColor: Colors.general.dark,
    },
    label:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        color: Colors.general.dark
    },
    inputWrapper:{
        marginTop:10,
        padding: 5,
        borderWidth: 1,
        borderColor: Colors.general.lightGray
    },
    input:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
    },
    infoInput:{
        lineHeight: 20,
        height: 100,
        flexWrap:'wrap'
    }
})