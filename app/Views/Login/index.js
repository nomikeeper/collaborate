import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Modal,
    KeyboardAvoidingView
} from 'react-native';
import { SafeAreaView, StackActions, NavigationActions } from "react-navigation";
import DropdownAlert from "react-native-dropdownalert";
import Icon from "react-native-vector-icons/Ionicons";
import KeyboardSpacer from "react-native-keyboard-spacer";
import { sessionService } from "redux-react-native-session";
import { connect } from "react-redux";
// ------ Importing Custom components
import Indicator from "../../Component/ActivityIndicator";
// ------ Importing Firebase
import Firebase from "react-native-firebase";
// ------ Import Routes
import Routes from "../../Navigator/Routes";
// ------ Importing styles
import styles from "./style";
// ------ Importing Api helper
import API from "../../API";
// ------ Importing Store
import Store from "../../Store";
// ------ Activity Indicator Actions
import ActivityActions from "../../Service/Actions/ActivityIndicator";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            email: '',
            password: '',
            passwordConfirm: '',
            modalVisibility: false,
        }
    }

    componentWillMount(){
        sessionService.refreshFromAsyncStorage
        sessionService.loadSession()
        .then(currentSession => {
            // console.log("Login: ", currentSession);
            if(currentSession){
                this._goToHomeScreen();
            }
        }).catch(err => {
            sessionService.initSessionService(Store);
        })

        API.setHeader({    
            'Accept': 'application/json',
            "Content-Type": "application/json",
        })
    }

    _goToHomeScreen(){
        const resetAction = StackActions.reset({
            index: 0,
            actions: [ NavigationActions.navigate({ routeName: Routes.Stack.Main}) ]
        })
        this.props.dispatch({
            type: ActivityActions.DEACTIVATE,
        })
        this.props.navigation.dispatch(resetAction);
    }

    // Validates the email
    _validateEmail(email){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    _login(){
        if(this.state.email && this.state.password){
            let data = {
                email: this.state.email,
                password: this.state.password,
            }

            if(this._validateEmail(this.state.email)){
                this.props.dispatch(dispatch => {
                    dispatch({
                        type: ActivityActions.ACTIVATE
                    })
                    API.POST(`/login`, JSON.stringify(data))
                    .then(resp => {
                        console.log("resp: ", resp)
                        sessionService.saveUser(resp).then(() => {
                            sessionService.saveSession(this.props.session);
                        })
                        setTimeout(() => {
                            this._goToHomeScreen();
                        },
                        500)
                    }).catch(err => {
                        dispatch({
                            type: ActivityActions.DEACTIVATE
                        })
                        console.log("Error: ", err);
                    })
                })
            }
            
        } else {
            this.onError("Username or password cannot be empty")
        }
    }

    _update(field, text){
        switch(field){
            case 'email':{
                this.setState({
                    email: text
                })
                break;
            }
            case 'password':{
                this.setState({
                    password: text
                })
                break;
            }
            case 'clear':{
                this.setState({
                    email: '',
                    password: '',
                })
                break;
            }
            default :{
                return;
            }
        }
    }

    // ...
    onError = error => {
        if (error) {
        this.dropdown.alertWithType('error', 'Error', error);
        }
    };
    // ...
    onSuccess = success => {
        if (success) {
        this.dropdown.alertWithType('success', 'Error', success);
        }
    };
    // ...
    onClose(data) {
        // data = {type, title, message, action}
        // action means how the alert was closed.
        // returns: automatic, programmatic, tap, pan or cancel
    }

    render(){
        return(
                <View style={ styles.wrapper }>
                    <KeyboardAvoidingView 
                        behavior={'padding'}
                        style={ styles.formWrapper }>
                        <View style={ styles.form }>
                            <View style={ styles.logoWrapper }>
                                <Text style={ styles.logo }>Collaborate</Text>
                            </View>
                            <View style={styles.field}>
                                <TextInput
                                    autoCapitalize={'none'}
                                    value={ this.state.email }
                                    clearTextOnFocus={true}
                                    clearButtonMode={'while-editing'}
                                    placeholder={'E-mail'} 
                                    style={ styles.input }
                                    onChangeText={(text) => { this._update('email', text)}}
                                />
                            </View>
                            <View style={styles.field}>
                                <TextInput
                                    autoCapitalize={'none'}
                                    value={ this.state.password}
                                    secureTextEntry={true}
                                    clearTextOnFocus={true}
                                    clearButtonMode={'while-editing'}
                                    placeholder={'Password'} 
                                    style={ styles.input }
                                    onChangeText={(text) => { this._update('password', text)}}
                                />
                            </View>
                            <View style={ styles.buttonWrapper }>
                                <TouchableOpacity
                                    style={ styles.buttonLogin }
                                    onPress={()=>{ this._login() }}
                                    >
                                    <View style={ styles.buttonLoginTextWrapper }>
                                        <Text style={ styles.buttonLoginText }>Login</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={ styles.buttonForgotPass}
                                    onPress={() => { this.props.navigation.navigate( Routes.Stack.ForgotPassword )}}
                                    >
                                    <View style={ styles.buttonForgotPassTextWrapper }>
                                        <Text style={ styles.buttonForgotPassText }>Forgot password?</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                    <View style={ styles.register }>
                        <TouchableOpacity
                            style={ styles.buttonRegister}
                            onPress={()=>{ 
                                this._update('clear');
                                this.props.navigation.navigate( Routes.Stack.Registration ) 
                            }}
                            >
                            <View style={ styles.buttonRegisterTextWrapper}>
                                <Text style={ styles.buttonRegisterText }>Register</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    
                    {/* !!! Make sure it's the last component in your document tree. */ }
                    <DropdownAlert ref={ref => this.dropdown = ref} onClose={data => this.onClose(data)}/>
                </View>
        )
    }
}

const mapStateToProps = (state) => ({
    session: state.Session,
    ActivityIndicator: state.ActivityIndicator
})

export default connect(mapStateToProps)(Index);