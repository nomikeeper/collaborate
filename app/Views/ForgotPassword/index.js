import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Alert,
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import DropdownAlert from "react-native-dropdownalert";
import KeyboardSpacer from "react-native-keyboard-spacer";
// ------ Import Routes
import Routes from "../../Navigator/Routes";
// ------ Importing styles
import styles from "./style";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            email: '',
        }
    }

    _update(field, text){
        switch(field){
            case 'email':{
                this.setState({
                    email: text
                })
                break;
            }
            default :{
                return;
            }
        }
    }
    onClose(data) {
        console.log("Data: ", data)
        if(data.type == 'success'){
            this.props.navigation.goBack()
        }
        // data = {type, title, message, action}
        // action means how the alert was closed.
        // returns: automatic, programmatic, tap, pan or cancel
    }

    render(){
        return(
            <View style={ styles.wrapper }>
                <View style={ styles.registerHeader }>
                    <TouchableOpacity
                        style={ styles.backButton }
                        onPress={() => { this.props.navigation.goBack() }}
                        >
                        <View style={ styles.backButtonIconWrapper }>
                            <Icon 
                                style={ styles.backButtonIcon }
                                name={'ios-arrow-back'}
                            />
                        </View>
                    </TouchableOpacity>
                    <Text style={ styles.registerHeaderTitle }>Forgot Password?</Text>
                </View>
                <View style={ styles.form }>
                    <View style={styles.field}>
                        <TextInput
                            value={ this.state.email }
                            clearTextOnFocus={true}
                            clearButtonMode={'while-editing'}
                            placeholder={'E-mail'} 
                            style={ styles.input }
                            onChangeText={(text) => { this._update('email', text)}}
                        />
                    </View>
                    <View style={ styles.buttonWrapper }>
                        <TouchableOpacity
                            style={ styles.buttonLogin }
                            onPress={()=>{ 
                                Alert.alert(
                                    "Password restoration",
                                    "It will send an e-mail to restore your password",
                                    [
                                        {text: 'Ok', onPress: () => { this.props.navigation.goBack()}}
                                    ]
                                )
                            }}
                            >
                            <View style={ styles.buttonLoginTextWrapper }>
                                <Text style={ styles.buttonLoginText }>Restore Password</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

export default Index;