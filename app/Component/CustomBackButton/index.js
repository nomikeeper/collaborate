import React, { Component } from "react";
import {
    View,
    TouchableOpacity
} from "react-native";
// ------ Importing Component
import Icon from "react-native-vector-icons/Ionicons";
// ------ Importing styles
import styles from "./style";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <TouchableOpacity
                style={[ 
                    styles.button,
                    this.props.buttonStyle 
                ]}
                onPress={() => { this.props.onPress ? this.props.onPress() : alert("No Functionality!")}}
                >
                <View style={[
                    styles.iconWrapper,
                    this.props.iconWrapper
                ]}>
                    <Icon 
                        style={[ 
                            styles.icon,
                            this.props.iconStyle
                        ]}
                        size={ this.props.iconSize ? this.props.iconSize : 24}
                        color={ this.props.color ? this.props.color : "#fff" }
                        name={ this.props.name ? this.props.name : 'ios-arrow-back'}
                    />
                </View>
            </TouchableOpacity>
        );
    }
}

export default Index;