import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";
// ------ Importing Colors
import Colors from "../../Theme/colors";

const { width, height } = Dimensions.get('window');

/* 
    Roboto Mono : 'Roboto Mono'
    Dancing Script: 'Dancing Script'

*/
export default StyleSheet.create({
    wrapper:{
        flex:1,
    },
    header:{
        flex:1,
        backgroundColor: Colors.stylish.tomato,
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray,
    },
    headerTitleBtn:{
        backgroundColor: Colors.general.white,
        marginBottom:10,
        padding:5,
        paddingHorizontal:10,
        borderRadius:6,
    },
    headerTitleBtnText:{
        fontFamily: "Montserrat",
    },
    logoWrapper:{
        flex:1,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        paddingLeft:20
    },
    logo:{
        //transform: [{ rotate: '-10deg' }],
        fontFamily: 'Great Vibes',
        color: Colors.general.white,
        fontSize:30
    },
    configButtonWrapper:{
        position: 'absolute',
        right: 0,
        bottom: 0,
    },
    configButton:{
        padding:5,
    },
    configButtonIconWrapper:{
        paddingRight:15,
    },
    configButtonIcon:{
        fontSize:30,
        color: Colors.general.white
    },
    content:{
        flex:9,
        backgroundColor: Colors.general.white,
    },
    item:{
        paddingVertical:10,
        paddingHorizontal:20,
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray,
    },
    itemContentWrapper:{
        flexDirection: 'row',
    },
    itemContent:{
        flex:9
    },
    itemIconWrapper:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemIcon:{
        fontSize:25,
        color: Colors.general.dark
    },
    titleWrapper:{
    },
    title:{
        fontFamily: "Montserrat",
        fontWeight: '700',
        fontStyle: 'italic',
        fontSize:16,
        color: Colors.stylish.tomato
    },
    categoryWrapper:{
        marginTop:5,
        flexDirection: 'row',
    },
    categoryLabel:{
        fontFamily: "Montserrat",
        fontWeight: '400',
        color: Colors.stylish.dark
    },
    category:{
        fontFamily: "Montserrat",
        fontWeight: "400",
        paddingLeft:10,
        color: Colors.general.dark
    },
    deadlineWrapper:{
        marginTop:5,
        flexDirection: 'row'
    },
    deadlineLabel:{
        fontFamily: "Montserrat",
        fontWeight: '400',
        color: Colors.general.dark
    },
    deadline:{
        fontFamily: "Montserrat",
        fontWeight: "400",
        paddingLeft:10,
        color: Colors.general.dark
    },
    addEventBtn:{
        position: 'absolute',
        bottom:25,
        right: 25,
        zIndex:5,
        padding:10,
        paddingHorizontal:12,
        borderRadius:30,
        backgroundColor: Colors.stylish.tomato,
    },
    addEventIconWrapper:{

    },
    addEventIcon:{

    },
    emptyWrapper:{
        flex:1,
        paddingHorizontal: width *0.15,
        justifyContent:'center',
        alignItems:'center'
    },
    emptyText:{
        fontFamily: 'Montserrat',
        fontSize:20,
        color: Colors.stylish.tomato,
        textAlign:'center',
        lineHeight:36,
    }
})

export const modal = StyleSheet.create({
    wrapper:{
        flex:1
    },
    content:{
        flex:1,
    },
    closeButton:{
        position: 'absolute',
        padding:5,
        zIndex:5,
        top:40,
        right:15,
    },
    filterWrapper:{
        flex:9,
        marginTop: Platform.OS == 'ios' ? 80 : 55,
        paddingHorizontal:20,
    },
    section:{
        flex:1,
        justifyContent: 'center',
        marginBottom:25,
        borderTopWidth: 1,
        borderColor: Colors.general.lightGray
    },
    buttonLogout:{
        borderWidth:1,
        borderColor: Colors.stylish.tomato,
        borderRadius:25,
        paddingVertical:10,
        marginHorizontal: width * 0.15,
    },
    buttonLogoutTextWrapper:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonLogoutText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        color: Colors.stylish.tomato,
    },
})