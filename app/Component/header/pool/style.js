import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";
// ------ Importing Colors
import Colors from "../../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper:{
        flex:1,
        width: width,
    },
    button:{
        paddingTop:40,
        alignItems: 'center',
        paddingBottom:10,
        backgroundColor: Colors.stylish.tomato,
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray
    },
    textWrapper:{

    },
    text:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        color: Colors.general.white,
    }
})