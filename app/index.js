import React, { Component } from 'react';
import {
    View,
    Text
} from 'react-native';
import { Provider, connect } from 'react-redux';
import { sessionService } from "redux-react-native-session";
// ------ Importing Activity Indicator
import Indicator from "./Component/ActivityIndicator";
// ------ Importing Navigator
import Navigator from "./Navigator"
// ------ Importing Store
import Store from "./Store";

sessionService.initSessionService(Store);

class Index extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <View style={{ flex:1 }}>
                <Navigator.Stack />

                {
                    this.props.ActivityIndicator.isActive ?
                        <Indicator />
                        :
                        null
                }
            </View>
        );
    }
}

// Redux state
const mapStateToProps = (state) => {
    return {
        navigatorState: state.Me,
        ActivityIndicator: state.ActivityIndicator
    }
}

// Connecting redux state to the App
const _App = connect(mapStateToProps)(Index);

// Main Application wrapped with Redux

export default Collaborate = () => {
    return(
        <Provider store={Store}>
            <_App />
        </Provider>
    );
}