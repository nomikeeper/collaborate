import React, { Component } from "react";
import {
    View,
    Text
} from "react-native";
// ------ Importing styles
import styles from "./style";
// ------ Importing Colors
import Colors from "../../Theme/colors";

class Index extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <View style={ styles.wrapper }>
                <Text style={ styles.text }>{this.props.children}</Text>
            </View>
        )
    }
}

export default Index;