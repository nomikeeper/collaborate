import { createStackNavigator } from 'react-navigation';
// ------ Importing Tab navigator
import Main from "./TabNavigator";
import Login from "../Views/Login";
import Registration from "../Views/Registration";
import ForgotPassword from "../Views/ForgotPassword";
import Favorite from "../Views/Favorite";
import Chat from "../Views/Chat";
import Request from "../Views/Request";
import CreateAd from "../Views/CreateAd"
import ProfileViewer from "../Views/ProfileViewer";
import EditProfile from "../Views/EditProfile";
import Ad from "../Views/Ad";
import MailForm from "../Views/MailForm";

const routes = {
    Main: Main,
    Login: Login,
    Registration: Registration,
    ForgotPassword: ForgotPassword,
    Chat: Chat,
    Request: Request,
    ProfileViewer: ProfileViewer,
    EditProfile: EditProfile,
    Favorite: Favorite,
    Ad: Ad,
    CreateAd: CreateAd,
    MailForm: MailForm
}

const config = {
    initialRouteName: "Login",
    navigationOptions:{
        header: null ,
    }
}

export default createStackNavigator(routes,config);