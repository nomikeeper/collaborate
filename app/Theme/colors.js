export default {
    general:{
        white: "#fff",
        black: "#000",
        dark: "#2a2a2a",
        light: "#eaeaea",
        lightGray: "#ddd",
        gray: "#999"
    },
    stylish:{
        tomato: "#D73A31"
    }
}