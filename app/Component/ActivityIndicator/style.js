import {
    StyleSheet,
    Dimensions
} from "react-native";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper:{
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex:1000,
        backgroundColor: "rgba(255,255,255,0.5)",
        justifyContent: 'center',
        alignItems: 'center',
        width:width,
        height: height,
    },
    indicator:{
    
    }
})