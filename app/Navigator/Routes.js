export default {
    Stack:{
        Login: "Login",
        Main: "Main",
        Registration: "Registration",
        ForgotPassword: "ForgotPassword",
        Chat: "Chat",
        Request: "Request",
        ProfileViewer: "ProfileViewer",
        EditProfile: 'EditProfile',
        Favorite: "Favorite",
        Ad: "Ad",
        CreateAd: "CreateAd",
        MailForm: "MailForm",
    },
    Tab:{
        Me: "Me",
        Pool: "Pool",
        Ads: "Ads",
        Inbox: "Inbox",
        Settings: "Settings"
    }
};