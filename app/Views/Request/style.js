import {
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';
// ------ Importing Colors
import Colors from "../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    outerWrapper:{
        flex:1,
    },
    wrapper:{
        flex:1,
        backgroundColor: Colors.general.white,
    },
    backButton:{
        paddingHorizontal:10,
    },
    content:{
        flex: 10,
    },
    section:{
        paddingHorizontal:20,
        marginVertical:10,
    },
    titleWrapper:{
        alignSelf: 'flex-start',
        borderBottomWidth:1,
        borderColor: Colors.general.dark,
    },
    title:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize: 20,
    },
    from:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize: 16,
    },
    sender:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize: 16,
        paddingLeft:10,
        fontStyle: 'italic',
        color: Colors.stylish.tomato
    },
    mailContext:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        lineHeight:18,
    },
    date:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        lineHeight:18,
    },
    control:{
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 10,
        borderTopWidth:1,
        borderColor: Colors.general.white,
    },
    controlBtn:{
        flex:1,
        marginVertical:5,
        marginBottom:25,
        marginHorizontal:5,
        justifyContent:'center',
        alignItems: 'center',
        borderWidth:1,
        borderColor: Colors.stylish.tomato,
        borderRadius:25,
        backgroundColor: Colors.general.white
    },
    controlBtnText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        color: Colors.stylish.tomato,
    }
})