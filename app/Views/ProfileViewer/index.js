import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    Dimensions,
    FlatList,
    Linking
} from "react-native";
import { connect } from "react-redux";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import Icon from "react-native-vector-icons/Ionicons";
// ------ Importing Routes
import Routes from "../../Navigator/Routes";
// ------ Importing styles
import styles from "./style";
// ------ Importing custom components
import CBack from "../../Component/CustomBackButton";
// ------ Importing API helper
import API from "../../API";
// ------ Importing Activity indicator actions
import ActivityIndicatorActions from "../../Service/Actions/ActivityIndicator";

const { height } = Dimensions.get('window');

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            initialFetch: false,
            profile: null,
        }
    }

    componentWillMount(){
        this.props.dispatch({ type: ActivityIndicatorActions.ACTIVATE })

        if(this.props.navigation.state.params.profile){
            this.setState({
                profile: this.props.navigation.state.params.profile,
                initialFetch: true
            },
            ()=>{
                this.props.dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
            })
        } else {
            const { id } = this.props.navigation.state.params;
            API.GET(`/profile/${id}`)
            .then(resp => {
                console.log("resp", resp)
                this.setState({
                    profile: resp,
                    initialFetch: true
                },
                ()=>{
                    this.props.dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                })
            }).catch(err => {
                this.props.dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                console.log("Error: ", err)
            })
        }
    }

    _selectImage(num){
        switch(num){
            case 1: {
                return require("../../Resources/Image/1.jpeg");
            }
            case 2: {
                return require("../../Resources/Image/2.jpg");
            }
            case 3: {
                return require("../../Resources/Image/5.jpg");
            }
            case 4: {
                return require("../../Resources/Image/4.jpg");
            }
            default:{
                return require("../../Resources/Image/3.jpg")
            }
        }
    }

    // if collaborate is enabled render collaborate button
    _collaborate(state){
        if(state == true){
            return(
                <View style={ styles.section }>
                    <TouchableOpacity
                        style={ styles.buttonLogout}
                        onPress={() => { 
                            console.log("PRops: ", this.props)
                            this.props.navigation.navigate(
                                Routes.Stack.MailForm,
                                {
                                    profile: this.props.navigation.state.params.profile
                                }
                            ) 
                        }}
                        >
                        <View style={ styles.buttonLogoutTextWrapper }>
                            <Text style={ styles.buttonLogoutText }>Collaborate</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    _like(state){
        if(state == true){
            return(
                <View style={[ styles.section, styles.marginTop25 ]}>
                    <TouchableOpacity
                        style={ styles.buttonLogout}
                        onPress={() => { 
                            const currentUser = this.props.session.user.profile;
                            console.log("Current User: ", currentUser);
                            console.log("Viewing User: ", this.state.profile);
                            this.props.dispatch(dispatch => {
                                dispatch({ type: ActivityIndicatorActions.ACTIVATE })
                                
                                API.POST(`/favorites/add`, JSON.stringify({
                                    owner: currentUser.uid,
                                    profile: this.state.profile
                                }))
                                .then(resp => {
                                    console.log("Resp: ", resp)
                                    dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                                }).catch(err => {
                                    console.log("Error: ", err)
                                    dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                                })
                            })
                        }}
                        >
                        <View style={ styles.buttonLogoutTextWrapper }>
                            <Text style={ styles.buttonLogoutText }>Like</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    _links(profile){
        if(profile.links.length > 0){
            return  <FlatList 
                        data={ profile.links}
                        renderItem={({item, index}) => this._renderLink(item,index)}
                    />
        } else {
            return <Text style={ styles.description }>No link.</Text>
        }
    }
    // ---- New functions

    // render single link
    _renderLink(item, index ){
        return(
            <TouchableOpacity
                key={ index }
                style={ styles.platformBtn }
                onPress={() => {
                    Linking.canOpenURL(item.link)
                    .then(supported => {
                        if(supported){
                            Linking.openURL( `https://${item.link.toLowerCase()}` ) 
                        }
                    }) 
                }}
                >
                <View style={ styles.platformBtnContent }>
                    <Icon 
                        style={ styles.platformIcon }
                        name={`logo-${item.icon}`}
                    />
                    <Text style={ styles.platformUsername }>{ item.name }</Text>
                </View>
            </TouchableOpacity>
        )
    }
    // background 
    _background(){
        if(!this.state.profile.background_photo_url){
            return(
                <Image 
                    style={ styles.coverImg}
                    source={require('../../Resources/Image/cover.jpg')}
                />
            )
        } else {
            return(
                    <Image 
                        style={[ styles.coverImg, { width: width, height: 300 } ]}
                        source={{ uri: this.state.photo_url}}
                    />
            )
        }
    }
    // -----

    render(){
        //console.log( "Navigation: ", this.props.navigation );
        const { profile, collaborate, profile_pic, like } = this.props.navigation.state.params
        //console.log("Profile: ", profile);
        return(
            <View style={ styles.wrapper }>
                {
                    this.state.initialFetch ?
                    <ParallaxScrollView
                        backgroundColor="transparent"
                        contentBackgroundColor="#FFF"
                        renderStickyHeader={() => (
                            <View style={ styles.stickyHeader }>
                                <Text style={ styles.stickyHeaderTitle }>{ this.state.profile.name }</Text>
                            </View>
                        )}
                        stickyHeaderHeight={ 70 }
                        contentContainerStyle={{flex: 1, minHeight: height * 0.6}}
                        parallaxHeaderHeight={ height * 0.4 }
                        renderForeground={() => (
                            <View style={ styles.header }>
                                <View style={ styles.cover }>
                                    {
                                        this._background()
                                    }
                                </View>
                                <View style={ styles.coverbg} />
                                <View style={styles.headerContent}>
                                    <View style={ styles.profilePicWrapper }>
                                        <Image 
                                            style={styles.profilePic}
                                            source={this._selectImage(profile_pic)}
                                        />
                                    </View>
                                    <View style={ styles.usernameWrapper }>
                                        <Text style={ styles.username }>{ this.state.profile.name }</Text>
                                    </View>
                                    <View style={ styles.titleWrapper }>
                                        <Text style={ styles.title}>{ this.state.profile.title }</Text>
                                    </View>
                                </View>
                            </View>
                        )}>
                            <View style={ styles.contentWrapper }>
                                <View style={ styles.content }>
                                    <View style={ styles.section }>
                                        <View style={ styles.labelWrapper} >
                                            <View style={ styles.labelBorder }>
                                                <Text style={ styles.label }>About me</Text>
                                            </View>
                                        </View>
                                        <View style={ styles.descriptionWrapper}>
                                            <Text style={ styles.description }>{ this.state.profile.about_me}</Text>
                                        </View>
                                    </View>

                                    <View style={[ styles.section, styles.marginTop25 ]}>
                                        <View style={ styles.labelWrapper} >
                                            <View style={ styles.labelBorder }>
                                                <Text style={ styles.label }>Interested In</Text>
                                            </View>
                                        </View>
                                        <View style={ styles.descriptionWrapper}>
                                            <Text style={ styles.description }>{ this.state.profile.interested_in }</Text>
                                        </View>
                                    </View>

                                    <View style={[ styles.section, styles.marginTop25 ]}>
                                        <View style={ styles.labelWrapper} >
                                            <View style={ styles.labelBorder }>
                                                <Text style={ styles.label }>Platforms</Text>
                                            </View>
                                        </View>
                                        <View style={ styles.platformsWrapper}>
                                            { this._links(this.state.profile) }
                                        </View>
                                    </View>
                                    {
                                        this._collaborate( collaborate )
                                    }
                                    {
                                        this._like( like )
                                    }
                                    <View style={[ styles.section, styles.marginTop25, { marginBottom:50,} ]}>
                                        <TouchableOpacity
                                            style={ styles.buttonLogout}
                                            onPress={() => { this.props.navigation.goBack() }}
                                            >
                                            <View style={ styles.buttonLogoutTextWrapper }>
                                                <Text style={ styles.buttonLogoutText }>Back</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                    </ParallaxScrollView>
                    :
                    <View />
                    }
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    session: state.Session
})
export default connect(mapStateToProps)(Index);