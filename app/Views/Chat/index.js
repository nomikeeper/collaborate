import React, { Component } from "react";
import {
    View,
    Text,
    Platform
} from "react-native";
import { GiftedChat, Message, Bubble } from "react-native-gifted-chat";
import Header from "../../Component/header/tomato";
// ------- Importing styles
import styles from "./style";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            messages: [],
        }
    }

    componentWillMount() {
        this.setState({
          messages: [
            {
              _id: 1,
              text: 'Hello developer',
              createdAt: new Date(),
              user: {
                _id: 2,
                name: 'React Native',
                avatar: 'https://placeimg.com/140/140/any',
              },
            },
          ],
        })
    }
    
    onSend(messages = []) {
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, messages),
        }))
    }

    render(){
        return(
            <View style={ styles.wrapper }>
                <Header
                    title={"Username"}
                    titleWrapperStyle={{
                        alignItems:'center',
                        paddingLeft:0,
                    }}
                    titleStyle={{
                        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
                        fontSize:18,
                        marginBottom:10,
                    }}
                    leftButton={true}
                    onPress={() => { this.props.navigation.goBack() }}
                />
                <View style={ styles.chat }>
                    <GiftedChat
                        messages={this.state.messages}
                        renderBubble={props => {
                            return (
                              <Bubble
                                {...props}
                                wrapperStyle={{
                                  right: styles.messageBubble
                                }}
                              />
                            );
                          }}
                        onSend={messages => this.onSend(messages)}
                        user={{
                            _id: 1,
                        }}
                    />
                </View>
            </View>
        );
    }
}

export default Index;