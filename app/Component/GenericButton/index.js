import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from "react-native";
// ------ Importing styles
import styles from "./style";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(

            <TouchableOpacity
                style={[ 
                    styles.button,
                    this.props.style
                ]}
                onPress={ this.props.onPress }
                >
                <View style={ styles.buttonTextWrapper }>
                    <Text style={ styles.buttonText }> { this.props.text } </Text>
                </View>
            </TouchableOpacity>
        )
    }
}

export default Index;