import {
    StyleSheet,
    Dimensions
} from "react-native";
// ------ Importing Colors
import Colors from "../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({ 
    wrapper:{
        flex:1,
    },
    chat:{
        flex:9,
    },
    messageBubble:{
        backgroundColor: Colors.stylish.tomato,
    }
})