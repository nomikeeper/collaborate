import {
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';

// ------ Importing Colors
import Colors from "../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper:{
        flex:1,
        backgroundColor: Colors.general.white,
    },
    header:{
        flex:1,
        backgroundColor: Colors.stylish.tomato
    },
    contentWrapper:{
        flex:9,
    },
    subHeader:{
        flex:1,
        flexDirection: 'row',
        paddingHorizontal: 10,
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray,
    },
    subMenu:{
        flex:1,
        marginVertical:5,
        marginHorizontal:5,
        justifyContent:'center',
        alignItems: 'center',
        borderWidth:1,
        borderColor: Colors.stylish.tomato,
        borderRadius:25,
        backgroundColor: Colors.general.white
    },
    subMenuText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        color: Colors.stylish.tomato,
    },
    activeSubMenu:{
        backgroundColor: Colors.stylish.tomato,
    },
    activeSubMenuText:{
        color: Colors.general.white
    },
    content:{
        flex:12,
    },
    mail:{
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray,
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    titleWrapper:{

    },
    title:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        fontWeight: 'bold',
    },
    fromWrapper:{
        marginTop:5,
        flexDirection: 'row'
    },
    from:{
        color: Colors.general.gray,
        fontSize:14,
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
    },
    whom:{
        marginLeft:10,
        color: Colors.stylish.tomato,
        fontSize:14,
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontStyle: 'italic',
    },
    chat:{
        flexDirection:'row',
        paddingHorizontal:15,
        paddingVertical:10,
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray
    },
    avatar:{
        flex:2,
        justifyContent:'center',
        alignItems:'center'
    },
    imageWrapper:{
        width:48,
        height:48,
        borderRadius:25,
        borderWidth:1,
        borderColor: Colors.general.dark,
        justifyContent:'center',
        alignItems:'center'
    },
    image:{
        width:50,
        height:50,
        borderRadius:25,
        resizeMode: 'cover',
    },
    chatInfo:{
        flex:8,
    },
    nameWrapper:{
        flex:1,
        justifyContent: 'center',
    },
    name:{
        fontFamily:Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:14,
        color: Colors.general.dark
    },
    lastMessageWrapper:{
        flex:1,
        justifyContent: 'center',
    },
    lastMessage:{
        fontFamily:Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:12,
        color: Colors.general.gray
    },
    emptyWrapper:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
        marginHorizontal: width * 0.15
    },
    emptyText:{
        fontFamily: 'Montserrat',
        fontSize:20,
        color: Colors.stylish.tomato,
        textAlign:'center',
    }
})