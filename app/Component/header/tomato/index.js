import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Platform
}  from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
// ------ Importinf Custom Components
import CBack from "../../CustomBackButton";
// ------ Importing styles
import styles from "./style";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <View style={ styles.header}>
                <View style={[ 
                    styles.logoWrapper,
                    this.props.titleWrapperStyle ? this.props.titleWrapperStyle : null
                    ]}>
                    {
                        this.props.isTitleButton ?
                            this.props.titleButton
                        :
                            <Text style={[ 
                                styles.logo,
                                this.props.titleStyle ? this.props.titleStyle : null
                                ]}>{this.props.title}
                            </Text>
                    }
                </View>
                {
                    this.props.rightButton ?
                        <View style={ styles.configButtonWrapper}>
                            <TouchableOpacity
                                onPress={() => { this.props.rightButton.onPress() }}
                                style={ styles.configButton }
                                >
                                <View style={ styles.configButtonIconWrapper }>
                                    <Icon 
                                        style={ styles.configButtonIcon }
                                        name={ this.props.rightButton.name }
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                        :
                        null
                }
                {
                    this.props.leftButton ?
                        <CBack
                            iconSize={ Platform.OS == 'ios' ? null : 30} 
                            buttonStyle={ styles.backButton }
                            onPress={() => this.props.onPress()}
                        />
                        :
                        null
                }
            </View>
        );
    }
}

export default Index;