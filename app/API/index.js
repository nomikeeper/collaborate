var HeaderParams = {}

class Api {

    // Set the header of the request
    static setHeader(header){
        if(header)
            HeaderParams = header;
    }

    // Clear the header of the request
    static clearHeader(){
        HeaderParams = {}
    }

    // Get header of the request
    static getHeader(){
        return HeaderParams;
    }

    // Get method
    static GET(_ROUTE){
        return this.xhr(_ROUTE, null, "GET");
    }
    // Post method
    static POST(_ROUTE, _PARAMS, _DATA){
        return this.xhr(_ROUTE, _PARAMS, "POST", _DATA);
    }
    // Put method
    static PATCH(_ROUTE, _PARAMS, _DATA){
        return this.xhr(_ROUTE,_PARAMS, "PATCH", _DATA);
    }

    static xhr(_ROUTE, _PARAMS, _METHOD, _DATA){
        const host = "https://collaborate-217522.appspot.com" //"http://localhost:8080" //
        const URL = `${host}${_ROUTE}`;

        // Setting the request's body
        let options = Object.assign(
            {},
            {   
                method: _METHOD,
                body: _PARAMS 
            }
        );

        options.headers = Api.getHeader() === {} ? null : Api.getHeader();

        // console.log("Options: ", options)
        // console.log("URL: ", URL)
        // Making the request
        return fetch(URL, options)
        .then((resp) => {
            // console.log("Plain Resp: ", resp)
            if(resp.status == 200){
                let respJson = resp.json();
                return respJson;
            } else {
                return {
                    ok:false,
                    status: resp.status,
                    message: resp.statusText,
                }
            }
        })
    }
}

export default Api;