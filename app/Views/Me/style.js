import {
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';

// ------ Importing Colors
import Colors from "../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper:{
        flex:1,
        backgroundColor: Colors.general.white,
    },
    header:{ 
        height: height * 0.3,
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    stickyHeader:{
        height:70,
        backgroundColor: Colors.stylish.tomato,
        paddingTop:40,
        paddingHorizontal:20,
        paddingBottom:10,
    },
    stickyHeaderTitle:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        color: Colors.general.white,
        fontSize:18,
        fontWeight: '300',
    },
    coverbg:{
        position: 'absolute',
        top:0,
        left:0,
        bottom:0,
        right:0,
        zIndex:2,
        backgroundColor: 'rgba(42,42,42,0.6)',
    },
    cover:{
        position: 'absolute',
        top:0,
        left:0,
        bottom:0,
        right:0,
        zIndex:1,
    },
    blankCover:{
        flex: 1,
        backgroundColor: Colors.stylish.tomato,
    },
    coverImg:{

    },
    headerContent:{
        zIndex:3,
    },
    profilePicSection:{
        //backgroundColor: '#424242'
    },
    profilePicWrapper:{
        backgroundColor: Colors.general.lightGray,
        alignSelf: 'center',
        width: 150,
        height: 150,
        borderRadius: 75, 
        overflow: 'hidden',
    },
    profilePic:{
        width:150,
        height:150,
        borderRadius:75,
    },
    usernameWrapper:{
        marginVertical:10,
        marginTop:25,
        justifyContent: 'center',
        alignItems: "center",
    },
    username:{
        color: Colors.general.white,
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Bold',
        fontSize:20,
        fontWeight: 'bold'
    },
    titleWrapper:{
        marginHorizontal:width * 0.1,
        marginVertical:5,
        justifyContent: 'center',
        alignItems: "center",
    },
    title:{
        color: Colors.general.white,
        textAlign: 'center',
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        lineHeight:24,
    },
    contentWrapper:{
        flex:1,
    },
    content:{
        paddingTop:50,
        paddingHorizontal:20,
        flex:1,
    },
    section:{
    },
    labelWrapper:{
        alignItems:'flex-start'
    },
    labelBorder:{
        borderBottomWidth: 1,
        borderColor: Colors.general.dark
    },
    label:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:20,
        lineHeight:30,
        borderBottomWidth:1,
    },
    descriptionWrapper:{
        marginTop:10,
    },
    description:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:14,
        lineHeight:24,
    },
    marginTop25:{
        marginTop:25,
    },
    marginVertical25:{
        marginVertical:25,
    },
    platformsWrapper:{
        paddingVertical:25,
    },
    platformBtn:{
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray
    },
    platformBtnContent:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'flex-start',
        paddingVertical:5,
    },
    platformIcon:{
        fontSize:30,
        lineHeight:30,
        margin:5,
    },
    platformUsername:{
        marginLeft:10,
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
    },
    buttonLogout:{
        borderWidth:1,
        borderColor: Colors.stylish.tomato,
        borderRadius:25,
        paddingVertical:10,
        marginHorizontal: width * 0.15,
    },
    buttonLogoutTextWrapper:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonLogoutText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        color: Colors.stylish.tomato,
    },
    imageChangeButton:{
        position: 'absolute',
        padding:5,
        bottom:-15,
        paddingHorizontal:10,
        borderWidth:1,
        borderColor: Colors.general.white,
        borderRadius:25,
        backgroundColor: "#424242",
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf:'center',
        zIndex:10000,
    },
    modal:{
        flex:1,
    },
    modalContent:{
        flex:9
    },
    modalCancel:{
        flex:1,
    },
    scrollview:{
        flex:1,
        marginHorizontal: width * 0.02,
        paddingTop:50,
        //backgroundColor: "#2a2a2a"
    },
    picturesWrapper:{
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    pictureWrapper:{
        width: width*0.3,
        marginRight: width * 0.02,
        borderRadius:8,
        minHeight:100,
        backgroundColor: Colors.stylish.tomato,
    },
    pictureButton:{
    },
    addPictureButton:{

    },
    pictureButtonImg:{
        resizeMode:'cover',
        width: width * 0.3, 
        minHeight: 100, 
        borderRadius:8,
    },
    activeIndicator:{
        position: 'absolute',
        bottom: 6,
        right: 10,
        zIndex:10,
    }
})