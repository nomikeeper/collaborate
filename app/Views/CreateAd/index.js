import React, { Component } from 'react';
import { 
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Alert,
    Platform
} from "react-native"; 
import Icon from "react-native-vector-icons/Ionicons";
import { connect } from "react-redux";
// ------ Importing Custom components
import GenericButton from "../../Component/GenericButton";
// ------ Importing styles
import styles from "./style";
// ------ Importing colors
import Colors from "../../Theme/colors";
// ------ Importing API helper
import API from "../../API";
import ActivityIndicatorActions from '../../Service/Actions/ActivityIndicator';

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            title: '',
            info: '',
            tag: '',
            deadline: '',
            isEdit: false,
            ad: '',
        }
    }
    static navigationOptions = ({ navigation }) => {
        const { edit } = navigation.state.params 
        return {
            header: false,
            title: edit ? 'Update Ad' : 'Create Ad',     
            headerTitleStyle:{
                fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
                color: "#FFF"
            },
            headerStyle: {
                backgroundColor: Colors.stylish.tomato,
            },
            headerLeft: <TouchableOpacity
                            style={ styles.backButton }
                            onPress={() => { navigation.goBack() }}
                            >
                            <View>
                                <Icon 
                                    size={ 30 }
                                    color={ '#FFF' }
                                    name={'ios-arrow-back'}
                                />
                            </View>
                        </TouchableOpacity>,
        }
    }

    componentWillMount(){
        if(this.props.navigation.state.params.edit){
            console.log("We are here.")
            const { ad, edit } = this.props.navigation.state.params;
            console.log("Ad: ", ad )
            console.log("Is editing: ", edit )
            this.setState({
                title: ad.data.title,
                info: ad.data.info,
                tag: ad.data.tag,
                deadline: ad.data.deadline,
                ad: ad,
                isEdit: edit
            })
        }
    }
    _update(field, value){
        switch(field){
            case fields.title: {
                this.setState({ title: value});
                break;
            }
            case fields.info:{
                this.setState({ info: value });
                break;
            }
            case fields.tag: {
                this.setState({ tag: value });
                break;
            }
            case fields.deadline:{
                this.setState({ deadline: value });
                break;
            }
            default: {
                return;
            }
        }
    }

    // Create Ad
    _createAd(){
        let newAd = this.state;
        newAd.profile_id = this.props.session.user.profile_id
        console.log("New Ad data: ", newAd)
        API.setHeader({
            "Content-type": "application/json"
        })
        API.POST("/create_ad", JSON.stringify(newAd))
        .then(resp => {
            console.log("Response: ", resp)
            this.props.navigation.state.params.refetchAds();
            this.props.navigation.goBack();
        }).catch(err => {
            console.log("Error: ", err)
        })
    }
    // Update Ad
    _updateAd(){
        let UpdatedAd = this.state.ad
        UpdatedAd.data.title = this.state.title ? this.state.title: '';
        UpdatedAd.data.info = this.state.info ? this.state.info : '';
        UpdatedAd.data.deadline = this.state.deadline ? this.state.deadline : '';
        UpdatedAd.data.tag = this.state.tag ? this.state.tag : '';

        console.log("Updated AD", UpdatedAd);
        this.props.dispatch(dispatch => {
            dispatch({ type: ActivityIndicatorActions.ACTIVATE})
            fetch("http://localhost:8080/update_ad",{
                method: "POST",
                headers:{
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(UpdatedAd)
            }).then(resp => resp.json())
            .then(resp => { 
                dispatch({ type: ActivityIndicatorActions.DEACTIVATE})
                UpdatedAd.data = resp.Status;
                this.props.navigation.state.params.updateAd(UpdatedAd)
                this.props.navigation.goBack();
                console.log("resp: ", resp)
            }).catch(err => {
                dispatch({ type: ActivityIndicatorActions.DEACTIVATE})
                console.log("Error: ", err)
            })
        })
    }

    // render function 
    render(){
        return(
            <View style={ styles.wrapper }>
                <ScrollView style={ styles.ScrollView }>
                    <View style={ styles.field }>
                        <View style={ styles.labelWrapper }>
                            <View style={ styles.labelBorder }>
                                <Text style={ styles.label }>Title</Text>
                            </View>
                        </View>
                        <View style={ styles.inputWrapper }>
                            <TextInput 
                                clearButtonMode={'while-editing'}
                                numberOfLines={1}
                                placeholder={ "Title" }
                                maxLength={ 60 }
                                style={ styles.input }
                                value={ this.state.title}
                                onChangeText={(value) => { this._update(fields.title, value )}}
                            />
                        </View>
                    </View>
                    <View style={ styles.field }>
                        <View style={ styles.labelWrapper }>
                            <View style={ styles.labelBorder }>
                                <Text style={ styles.label }>Info</Text>
                            </View>
                        </View>
                        <View style={ styles.inputWrapper }>
                            <TextInput 
                                clearButtonMode={'while-editing'}
                                multiline={true}
                                placeholder={ "Info" }
                                maxLength={ 250 }
                                style={[ styles.input, styles.infoInput ]}
                                value={ this.state.info}
                                onChangeText={(value) => { this._update(fields.info, value )}}
                            />
                        </View>
                    </View>

                    <View style={ styles.field }>
                        <View style={ styles.labelWrapper }>
                            <View style={ styles.labelBorder }>
                                <Text style={ styles.label }>Tag</Text>
                            </View>
                        </View>
                        <View style={ styles.inputWrapper }>
                            <TextInput 
                                clearButtonMode={'while-editing'}
                                numberOfLines={1}
                                placeholder={ "Tag" }
                                maxLength={ 100 }
                                style={[ styles.input, { flexWrap: "wrap", alignItems: "flex-start"} ]}
                                value={ this.state.tag }
                                onChangeText={(value) => { this._update(fields.tag, value )}}
                            />
                        </View>
                    </View>

                    <View style={ styles.field }>
                        <View style={ styles.labelWrapper }>
                            <View style={ styles.labelBorder }>
                                <Text style={ styles.label }>Deadline</Text>
                            </View>
                        </View>
                        <View style={ styles.inputWrapper }>
                            <TextInput 
                                clearButtonMode={'while-editing'}
                                numberOfLines={1}
                                placeholder={ "Deadline" }
                                maxLength={ 100 }
                                style={[ styles.input ]}
                                value={ this.state.deadline }
                                onChangeText={(value) => { this._update(fields.deadline, value )}}
                            />
                        </View>
                    </View>
                    <View>
                        <GenericButton 
                            onPress={() => { 
                                this.state.isEdit ?
                                    this._updateAd()
                                    :
                                    this._createAd()
                            }}
                            text={ this.state.isEdit ? 'Update' : 'Create'}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    session: state.Session
})

export default connect(mapStateToProps)(Index);

const fields = {
    title: 'title',
    info: 'info',
    deadline: 'date',
    tag: 'category',
}