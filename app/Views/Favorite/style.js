import {
    StyleSheet,
    Dimensions,
    Platform,
} from "react-native";
// ------ Importing Colors
import Colors from "../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper:{
        flex:1,
    },
    content:{
        flex: 9,
        backgroundColor: Colors.general.white,
    },
    chat:{
        flexDirection:'row',
        paddingHorizontal:15,
        paddingVertical:10,
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray
    },
    avatar:{
        flex:2,
        justifyContent:'center',
        alignItems:'center'
    },
    imageWrapper:{
        width:48,
        height:48,
        borderRadius:25,
        borderWidth:1,
        borderColor: Colors.general.dark,
        justifyContent:'center',
        alignItems:'center'
    },
    image:{
        width:50,
        height:50,
        borderRadius:25,
        resizeMode: 'cover',
    },
    chatInfo:{
        flex:8,
    },
    nameWrapper:{
        flex:1,
        justifyContent: 'center',
    },
    name:{
        fontFamily:Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:14,
        color: Colors.general.dark
    },
    lastMessageWrapper:{
        flex:1,
        justifyContent: 'center',
    },
    lastMessage:{
        fontFamily:Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:12,
        color: Colors.general.gray
    }
})

export const modal = StyleSheet.create({
    wrapper:{
        flex:1
    },
    content:{
        flex:1,
    },
    closeButton:{
        position: 'absolute',
        zIndex: 5,
        padding:5,
        top:40,
        right:15,
    },
    filterWrapper:{
        flex:9,
        marginTop: Platform.OS == 'ios' ? 80 : 55,
        paddingHorizontal:20,
    },
    section:{
        flex:1,
        justifyContent: 'center',
        marginBottom:25,
        borderTopWidth: 1,
        borderColor: Colors.general.lightGray
    },
    buttonLogout:{
        borderWidth:1,
        borderColor: Colors.stylish.tomato,
        borderRadius:25,
        paddingVertical:10,
        marginHorizontal: width * 0.15,
    },
    buttonLogoutTextWrapper:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonLogoutText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        color: Colors.stylish.tomato,
    },
})