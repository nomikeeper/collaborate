import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    Image,
    Modal,
    Platform
} from 'react-native';
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/Ionicons";
// ------ Importing custom components
import Header from "../../Component/header/tomato";
import Empty from "../../Component/Empty";
// ------ Importing styles 
import styles, { modal } from "./style";
// ------ Importing Colors
import Colors from "../../Theme/colors";
// ------ Importing Routes
import Routes from "../../Navigator/Routes";
// ------ Importing API Helper
import API from "../../API";
// ------ Importing Activity indicator actions
import ActivityIndicatorActions from "../../Service/Actions/ActivityIndicator";

class Index extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: [
                {
                    image: '../../Resources/Image/cover.jpg',
                    name: 'Player one'
                },
                {
                    image: '../../Resources/Image/cover.jpg',
                    name: 'Player two'
                },
                {
                    image: '../../Resources/Image/cover.jpg',
                    name: 'Player three'
                },
                {
                    image: '../../Resources/Image/cover.jpg',
                    name: 'Player four'
                },
            ],
            list:[],
            modalVisibility: false,
        }
    }

    componentWillMount(){
        this.props.dispatch(dispatch => {
            dispatch({ type: ActivityIndicatorActions.ACTIVATE })
            let { uid } = this.props.session.user.profile
            // console.log(`UID: ${uid}`)
            //API.GET(`/favorites/${uid}`)
            fetch(`http://localhost:8080/favorites/${uid}`,{
                method: 'GET'
            }).then(resp => resp.json())
            .then(resp => {
                console.log("resp: ", resp)
                dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                this.setState({ list: resp.Favorites })
            }).catch(err => {
                dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                console.log("Error: ", err)
            })
        })
        
    }

    _selectImage(num){
        switch(num){
            case 1: {
                return require("../../Resources/Image/1.jpeg");
            }
            case 2: {
                return require("../../Resources/Image/2.jpg");
            }
            case 3: {
                return require("../../Resources/Image/5.jpg");
            }
            case 4: {
                return require("../../Resources/Image/4.jpg");
            }
            default:{
                return require("../../Resources/Image/3.jpg")
            }
        }
    }

    _openModal(){
        this.setState({ modalVisibility: true })
    }

    _closeModal(){
        this.setState({ modalVisibility: false })
    }

    _renderItem(item, index){
        return  <TouchableOpacity
                    key={ index }
                    onPress={()=>{ 
                        this.props.navigation.navigate( Routes.Stack.ProfileViewer, 
                                { 
                                    name: item.name,
                                    collaborate: true,
                                    profile: item,
                                    profile_pic: index
                                })
                    }}>
                    <View style={ styles.chat }>
                        <View style={ styles.avatar}>
                            <View style={ styles.imageWrapper }>
                                <Image 
                                    style={ styles.image}
                                    source={ this._selectImage(index) }
                                />
                            </View>
                        </View>
                        <View style={ styles.chatInfo }>
                            <View style={ styles.nameWrapper }>
                                <Text style={ styles.name }>{ item.name }</Text>
                            </View>
                            <View style={ styles.lastMessageWrapper }>
                                <Text style={ styles.lastMessage }>New wave is coming.</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
    }

    // Content
    _content(){
        if(this.state.list && this.state.list.length > 0){
            return(
                <FlatList 
                    style={{ flex:1 }}
                    data={ this.state.list }
                    renderItem={({item, index}) => this._renderItem(item,index)}
                />
            )
        } else {
            return(
                <Empty>
                    There is favorite content creator yet.
                </Empty>
            )
        }
    }

    render(){
        return(
            <View style={ styles.wrapper }>
                <Header 
                    title={"Favorites Users"}
                    titleWrapperStyle={{
                        alignItems:'center',
                        paddingLeft:0,
                    }}
                    titleStyle={{
                        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
                        fontSize:18,
                        marginBottom:10,
                    }}
                    rightButton={{
                        name: 'ios-settings',
                        onPress: () => { this._openModal() }
                    }}
                    leftButton={true}
                    onPress={() => { this.props.navigation.goBack() }}
                />
                <View style={ styles.content }>
                    { this._content() }
                </View>
                <Modal
                    style={ modal.wrapper }
                    visible={ this.state.modalVisibility}
                    animationType={'slide'}
                    >
                    <View style={ modal.content }>
                        <TouchableOpacity
                            style={ modal.closeButton }
                            onPress={()=>{ this._closeModal()}}
                            >
                            <View>
                                <Icon 
                                    size={ 40 }
                                    name="ios-close-circle-outline"
                                    color={ Colors.stylish.tomato }
                                />
                            </View>
                        </TouchableOpacity>
                        <View style={ modal.filterWrapper }>
                            <Text>Filters will be listed here</Text>
                        </View>
                        <View style={ modal.section }>
                            <TouchableOpacity
                                style={ modal.buttonLogout}
                                onPress={() => { 
                                    this._closeModal()
                                }}
                                >
                                <View style={ modal.buttonLogoutTextWrapper }>
                                    <Text style={ modal.buttonLogoutText }>Apply filter</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    session: state.Session
})
export default connect(mapStateToProps)(Index);