import {
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';
// ------ Importing colors
import Colors from "../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper:{
        flex:1,
        backgroundColor: Colors.general.white,
    },
    scrollView:{
        flex:1,
        backgroundColor: Colors.general.white,
        paddingBottom:50,
    },
    backButton:{
        position: 'absolute',
        zIndex:3,
        left: 10,
        paddingHorizontal:10,
    },
    field:{
        paddingHorizontal: 20,
        marginVertical: 10,
    },
    labelWrapper:{
        alignItems: 'flex-start'
    },
    labelBorder:{
        borderBottomWidth: 2,
        borderColor: Colors.general.dark,
    },
    label:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        color: Colors.general.dark
    },
    inputWrapper:{
        marginTop:10,
        padding: 5,
        borderWidth: 1,
        borderColor: Colors.general.lightGray
    },
    input:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
    },
    aboutMe:{
        lineHeight: 20,
        height: 100,
    },
    platformsWrapper:{
        paddingVertical:25,
    },
    platformBtn:{
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray
    },
    platformBtnContent:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'flex-start',
        paddingVertical:5,
    },
    platformIcon:{
        fontSize:30,
        lineHeight:30,
        margin:5,
    },
    platformCloseBtn:{
        position:'absolute',
        right:0,
        color: Colors.general.dark,
        zIndex:5,
        fontSize:40,
        lineHeight:40,
        paddingHorizontal: 10,
    },
    platformUsername:{
        marginLeft:10,
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
    },
    addIconWrapper:{
        paddingVertical:10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    addIcon:{
        
    },
    optionBtnsWrapper:{

    },
    description:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
    }
})

export const modal = StyleSheet.create({
    wrapper:{
        flex:1,
    },
    closeBtn:{
        position: 'absolute',
        top: Platform.OS == 'ios' ? 35 : 40,
        right: 15,
        padding:5,
        zIndex:5,
    },
    content:{
        flex:1,
        paddingTop:50,
        backgroundColor: Colors.general.white,
    },
    titleWrapper:{
        paddingHorizontal:20,
    },
    title:{
        fontSize:20,
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontWeight: 'bold',
        color: Colors.stylish.tomato
    },
    selectIconLabel:{
        fontSize:16,
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        color: Colors.general.dark
    },
    selectIconBtn:{
        marginLeft: 10,
    },
    addBtnWrapper:{
        marginTop:25,
    },
    iconsPopUp:{
        position: 'absolute',
        width:width,
        height:height,
        top:0,
        left:0,
        justifyContent:'center',
        alignItems: 'center', 
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    chooseIconContent:{
        backgroundColor: Colors.general.white,
        width: width* 0.8,
        borderRadius:8,
        paddingVertical:15,
    },
    chooseIconTitleWrapper:{
        alignItems: 'center'
    },
    chooseIconTitleText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:20,
        fontWeight: 'bold',
        marginVertical:10,
    },
    iconsWrapper:{
        justifyContent: 'center',
        flexDirection:'row',
        flexWrap: 'wrap',
    },
    icon:{
        marginHorizontal:10,
        marginVertical:5
    },
    cancelWrapper:{
        width: width * 0.8,
        marginTop:20,
        marginBottom:15,
    }
})