import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from "react-native";
// ------ Importing styles
import styles from "./style";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <View style={ styles.wrapper }>
                <TouchableOpacity
                    style={ styles.button}
                    onPress={()=>{ alert('Open list')}}
                    >
                    <View style={ styles.textWrapper }>
                        <Text style={ styles.text }>Library</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

export default Index;

/* 

             <View style={ styles.wrapper }>
                <Touchableopacity
                    style={ styles.button}
                    onPress={()=>{ alert('Open list')}}
                    >
                    <View style={ styles.textWrapper }>
                        <Text style={ styles.text }>Pool header</Text>
                    </View>
                </Touchableopacity>
            </View>
*/