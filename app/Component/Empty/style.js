import {
    StyleSheet,
    Platform,
    Dimensions
} from "react-native";
// ------ Importing colors
import Colors from "../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
        marginHorizontal: width * 0.15
    },
    text:{
        fontFamily: 'Montserrat',
        fontSize:20,
        color: Colors.stylish.tomato,
        textAlign:'center',
    }
})