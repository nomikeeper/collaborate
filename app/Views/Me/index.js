import React, { Component } from 'react';
import {
    ScrollView,
    View,
    Text,
    Image,
    TouchableOpacity,
    Dimensions,
    Platform,
    Modal,
    Linking
} from 'react-native';
import { SafeAreaView, StackActions, NavigationActions } from 'react-navigation';
import FetchBlob from "rn-fetch-blob";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import Icon from "react-native-vector-icons/Ionicons";
import ImagePicker from "react-native-image-picker";
import { sessionService } from "redux-react-native-session";
import { connect } from "react-redux";
// ------ Importing custom components
import GenericButton from "../../Component/GenericButton";
// ------ Importing styles
import styles from "./style";
// ------ Importing Colors
import Colors from "../../Theme/colors";
// ------ Importing Routes
import Routes from "../../Navigator/Routes";
import API from '../../API';
// ------ Importing Activity Indicator Actions
import ActivityIndicatorActions from "../../Service/Actions/ActivityIndicator";

const { width, height } = Dimensions.get('window');
const blob = FetchBlob.polyfill.Blob;
window.Blob = blob;

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            user: '',
            isLoading: true,
            avatarSource: null,
            modalVisibility: false,
        }
    }

    // React navigations props
    static navigationOptions= ({ navigation }) => {
        return {
            title: "Account",
            tabBarIcon: ({ focused, tintColor }) => {
                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return <Icon name={'ios-contact'} size={25} color={tintColor} />;
            },
        }
    }

    componentWillReceiveProps(nextProps){
        //console.log("Next Props: ", nextProps);
        if(this.state.user != nextProps.session.user){
            this.setState({
                user: nextProps.session.user
            })
        }
    }

    componentWillMount(){
        sessionService.refreshFromAsyncStorage
        sessionService.loadSession()
        .then(currentSession => {
            //console.log(currentSession)
            this.props.dispatch({ type: ActivityIndicatorActions.ACTIVATE })
                this.setState({
                    user: currentSession.user,
                    isLoading: false,
                }, () => {
                    this.props.dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                })
            

        }).catch(err => {
            console.log("Error: ", err)
        })
    }

    _links(){
        if(this.state.user.profile.links.length > 0 && !this.state.isLoading){
            return this.state.user.profile.links.map( (item, index) => {
                return this._renderLink(item, index);
            })
        } else {
            return(
                <Text style={ styles.description }>No links at the moment</Text>
            )
        }
    }

    // render single link
    _renderLink(item, index ){
        // console.log('Item: ', item)
        return(
            <TouchableOpacity
                key={ index }
                style={ styles.platformBtn }
                onPress={() => {
                    Linking.canOpenURL(item.link)
                    .then(supported => {
                        if(supported){
                            Linking.openURL( `https://${item.link.toLowerCase()}` ) 
                        }
                    }) 
                }}
                >
                <View style={ styles.platformBtnContent }>
                    <Icon 
                        style={ styles.platformIcon }
                        name={`logo-${item.icon}`}
                    />
                    <Text style={ styles.platformUsername }>{ item.name }</Text>
                </View>
            </TouchableOpacity>
        )
    }

    _logout(){
        sessionService.deleteUser();
        sessionService.deleteSession();
        const resetAction = StackActions.reset({
            index: 0,
            actions: [ NavigationActions.navigate({ routeName: Routes.Stack.Login}) ]
        })
        this.props.navigation.dispatch(resetAction);
    }

    _background(){
        if(!this.state.user.profile.background_photo_url){
            return(
                <View style={ styles.blankCover } />
            )
        } else {
            return(
                    <Image 
                        style={[ styles.coverImg, { width: width, height: 300 } ]}
                        source={{ uri: this.state.photo_url}}
                    />
            )
        }
    }
    // Upload the image
    _uploadImage(Image, isBackground){
        var _formData = new FormData()
        _formData.append("file", Image)

        let header = isBackground ? {
                'Accept':'application/json',
                'Content-Type': 'multipart/form-data',
                'uid': this.state.user.profile.uid,
                'background': true
            } 
            :
            {
                'Accept':'application/json',
                'Content-Type': 'multipart/form-data',
                'uid': this.state.user.profile.uid
            }
        API.setHeader(header);
        API.POST("/upload_profile_picture",_formData)
        .then(response => {
            API.clearHeader();
            let updatedUserInfo = Object.assign({}, this.state.user);
            updatedUserInfo.profile = response;
            sessionService.saveUser(updatedUserInfo)
            .then(() => {
                sessionService.loadUser(user => {
                    sessionService.saveSession(this.props.session)
                    .then(() => {
                        this.setState({ user: user })
                    })
                })
            })
        }).catch(err => {
            console.log("Error: ", err)
        })
    }
    // Pick image
    _pickImage(){
        ImagePicker.showImagePicker((response) => {
            //console.log('Response = ', response);
          
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                /*
                const source = { uri: 'data:image/jpeg;base64,' + response.data} //response.uri };
                this.setState({
                    avatarSource: source,
                });
                */
                const Image = {
                    uri: response.uri,
                    type: response.type,
                    name: response.fileName
                }
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                // this._uploadImage(Image, response.uri, response.fileName)
                this._uploadImage(Image);
                
            }
        });
    }
    // Avatar Image
    _avatarImage(){
        if(this.state.user.profile.photo_url){
            return(
                <Image 
                    style={styles.profilePic}
                    source={{ uri: this.state.user.profile.photo_url }}
                />
            )
        } else if( this.state.avatarSource) 
        {
            return(
                <Image 
                    style={styles.profilePic}
                    source={ this.state.avatarSource }
                />
            )
        }
        else {
            return(
                <View style={[ 
                        styles.profilePic, 
                        { 
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: Colors.general.white 
                        }
                    ]}>
                    <Icon 
                        name={'ios-camera'}
                        size={60}
                        color={ Colors.general.gray }
                    />
                </View>
            )
        }
    }
    // Avatar
    _avatar(){
        return(
            <View>
                { this._avatarImage() }
            </View>
        )
    }

    // Enable Modal
    _enableModal(){
        this.setState({
            modalVisibility: true
        })
    }
    // Disable Modal
    _disableModal(){
        this.setState({
            modalVisibility: false
        })
    }

    // Set Profile Picture
    _setProfilePicture(imageLink){
        if(imageLink != this.state.user.profile.photo_url ){
            this.props.dispatch({ type: ActivityIndicatorActions.ACTIVATE})
            // Change profile picture and update the value of it.
            let updatedProfile = this.state.user.profile;
            updatedProfile.photo_url = imageLink;
            let data = {
                profile: updatedProfile,
                id: this.state.user.profile_id
            }
            API.setHeader({
                "Content-Type": "application/json; charset=utf-8"
            })
            API.POST("/set_profile_picture",JSON.stringify(data))
            .then(resp => {
                let updatedUserInfo = Object.assign({}, this.state.user);
                updatedUserInfo.profile = resp;
                this.props.dispatch(dispatch => {
                    sessionService.saveUser(updatedUserInfo).then(() => {
                        sessionService.loadUser().then(user => {
                            sessionService.saveSession(this.props.session)
                            .then(() => {
                                this.setState({ 
                                    user: user, 
                                    modalVisibility: false 
                                },
                                ()=> {
                                    dispatch({ type: ActivityIndicatorActions.DEACTIVATE})
                                })   
                            })
                        })
                    })
                })
            }).catch(err => {
                console.log("Error: ", err)
            })
            //API.POST("//change_profile_picture")
        }
    }
    // Profile pictures
    _profile_pictures(){
        if( !this.state.loading && this.state.user 
            && this.state.user.profile.profile_pictures
            && this.state.user.profile.profile_pictures.length > 0){
            let profile_pictures = this.state.user.profile.profile_pictures;

            return profile_pictures.map((item, index) => {
                return  <View key={index} 
                            style={styles.pictureWrapper}>
                            <TouchableOpacity
                                onPress={() => { this._setProfilePicture(item) }}
                                >
                                <Image 
                                    style={ styles.pictureButtonImg }
                                    source={{uri: item}}
                                />
                                
                            </TouchableOpacity>
                            {
                                this.state.user.profile.photo_url == item ?
                                    <Icon 
                                        name={"ios-checkmark-circle-outline"}
                                        size={ 30 }
                                        color={ Colors.general.white }
                                        style={ styles.activeIndicator }    
                                    />
                                    :
                                    null
                            }
                        </View>
            })
        }
    }

    // Content
    _content(){
        if(this.state.isLoading){
            return <Text>Loading</Text>
        } else {
            return(
                <ParallaxScrollView
                    backgroundColor="transparent"
                    contentBackgroundColor="#FFF"
                    renderStickyHeader={() => (
                        <View style={ styles.stickyHeader }>
                            <Text style={ styles.stickyHeaderTitle }>{ this.state.user.profile.name }</Text>
                        </View>
                    )}
                    stickyHeaderHeight={ 70 }
                    contentContainerStyle={{flex: 1, minHeight: height * 0.6}}
                    parallaxHeaderHeight={ height * 0.4 }
                    renderForeground={() => (
                        <View style={ styles.header }>
                            <View style={ styles.cover }>
                                {
                                    this._background()
                                }
                            </View>
                            <View style={ styles.coverbg} />
                            <View style={styles.headerContent}>
                                <View style={ styles.profilePicSection }>
                                    <TouchableOpacity
                                            style={ styles.imageChangeButton }
                                            onPress={() => {
                                                this._enableModal()
                                            }}
                                        >
                                        <View>
                                            <Icon 
                                                name={'ios-repeat'}
                                                color={ Colors.general.white }
                                                size={24}
                                            />
                                        </View>
                                    </TouchableOpacity>
                                    <View style={ styles.profilePicWrapper }>
                                        {
                                                this._avatar()
                                        }
                                    </View>
                                </View>
                                <View style={ styles.usernameWrapper }>
                                    <Text style={ styles.username }>
                                        { 
                                            this.state.user.profile.name  ?
                                                this.state.user.profile.name 
                                                :
                                                "The name is not set"
                                        }
                                    </Text>
                                </View>
                                <View style={ styles.titleWrapper }>
                                    <Text style={ styles.title}>
                                        {
                                            this.state.user.profile.title ?
                                                this.state.user.profile.title
                                                :
                                                "The title is not set"
                                        }
                                        
                                    </Text>
                                </View>
                            </View>
                        </View>
                    )}>
                        <View style={ styles.contentWrapper }>
                            <View style={ styles.content }>
                                <View style={ styles.section }>
                                    <View style={ styles.labelWrapper} >
                                        <View style={ styles.labelBorder }>
                                            <Text style={ styles.label }>About me</Text>
                                        </View>
                                    </View>
                                    <View style={ styles.descriptionWrapper}>
                                        <Text style={ styles.description }>
                                            {
                                                this.state.user.profile.about_me ?
                                                    this.state.user.profile.about_me
                                                    :
                                                    "The about me is not set."
                                            }
                                        </Text>
                                    </View>
                                </View>

                                <View style={[ styles.section, styles.marginTop25 ]}>
                                    <View style={ styles.labelWrapper} >
                                        <View style={ styles.labelBorder }>
                                            <Text style={ styles.label }>Interested In</Text>
                                        </View>
                                    </View>
                                    <View style={ styles.descriptionWrapper}>
                                        <Text style={ styles.description }>
                                            {
                                                this.state.user.profile.interested_in ?
                                                    this.state.user.profile.interested_in
                                                    :
                                                    "The interest is not set."
                                            }
                                        </Text>
                                    </View>
                                </View>

                                <View style={[ styles.section, styles.marginTop25 ]}>
                                    <View style={ styles.labelWrapper} >
                                        <View style={ styles.labelBorder }>
                                            <Text style={ styles.label }>Links</Text>
                                        </View>
                                    </View>
                                    <View style={ styles.platformsWrapper}>
                                        {
                                            this._links()
                                        }
                                    </View>
                                </View>
                                <View style={ styles.section }>
                                    <GenericButton 
                                        onPress={() => { this.props.navigation.navigate(Routes.Stack.EditProfile, { name: this.state.user.profile.name }) }}
                                        text={'Edit Profile'}
                                    />
                                </View>
                                <View style={[ styles.section, styles.marginVertical25 ]}>
                                    <GenericButton 
                                        onPress={() => { this._logout() }}
                                        text={'Log out'}
                                    />
                                </View>
                            </View>
                        </View>
                </ParallaxScrollView>
            )
        }
    }

    render(){
        // console.log("This.state: ", this.state.user)
        return(
            <View style={ styles.wrapper }>
                { this._content() }
                <Modal
                    animationType='slide'
                    visible={this.state.modalVisibility}
                    style={ styles.modal }
                    >
                    <View style={ styles.modalContent}>
                        <ScrollView style={ styles.scrollview }>
                            <View style={ styles.picturesWrapper }>
                                {
                                    this._profile_pictures()
                                }
                                <View style={[ 
                                        styles.pictureWrapper, 
                                        {
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        } 
                                    ]}>
                                    <TouchableOpacity
                                        style={ styles.addPictureButton }
                                        onPress={() => { this._pickImage() }}
                                        >
                                        <Icon 
                                            name={"ios-add"}
                                            color={ Colors.general.white }
                                            size={60}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                    <View style={ styles.modalCancel }>
                        <GenericButton 
                            onPress={() => { this._disableModal() }}
                            text={'Cancel'}
                        />
                    </View>
                </Modal>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    session: state.Session
})

export default connect(mapStateToProps)(Index);