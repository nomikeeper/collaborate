import { Platform, Dimensions } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
// ------ Importing Views
import Me from "../Views/Me";
import Pool from "../Views/Pool";
import Ads from "../Views/Ads";
import Inbox from "../Views/Inbox";
import Settings from "../Views/Settings";
// ------ Importing Colors
import Colors from "../Theme/colors";

const { height } = Dimensions.get('window');
const routes = {
    Me: Me,
    Pool: Pool,
    Ads: Ads,
    Inbox: Inbox,
    Settings: Settings,
}

const config = {
    tabBarOptions:{
        showIcon: true,
        activeTintColor: Colors.stylish.tomato,
        inactiveTintColor: Colors.general.lightGray,
        style:{
            height: height * 0.07,
            paddingBottom: height * 0.02
        },
        iconStyle:{
            fontSize:14,
        },
        labelStyle:{
            fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
            fontWeight: '400',
            fontSize:10,
        },
        indicatorStyle: {
            backgroundColor: 'transparent'
        },
    }
}

export default createBottomTabNavigator(routes, config);