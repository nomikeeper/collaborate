import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";
// ------ Importing Colors
import Colors from "../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    button:{
        borderWidth:1,
        borderColor: Colors.stylish.tomato,
        borderRadius:25,
        paddingVertical:10,
        marginHorizontal: width * 0.15,
    },
    buttonTextWrapper:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        color: Colors.stylish.tomato,
    },
})