import {
    StyleSheet,
    Dimensions,
    Platform,
} from "react-native";

// ------ Import colors
import Colors from "../../Theme/colors";

const { width, height} = Dimensions.get('window');

export default StyleSheet.create({
    outerWrapper:{
        flex:1,
        backgroundColor: Colors.general.white,
    },
    wrapper:{
        flex:1,
        backgroundColor: Colors.general.white,
    },
    registerHeader:{
        height: height * 0.1,
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    registerHeaderTitle:{
        fontSize:20,
        color: Colors.stylish.tomato,
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        paddingBottom:10,
    },
    backButton:{
        position:'absolute',
        left:0,
        paddingHorizontal:20,
        paddingVertical:5,
    },
    backButtonIconWrapper:{

    },
    backButtonIcon:{
        fontSize:30,
        color: Colors.stylish.tomato
    },
    form:{
        height: height * 0.9,
        paddingHorizontal: width * 0.1,
        paddingTop: height * 0.05,
    },
    logoWrapper:{
        alignItems: 'center',
        marginBottom:20
    },
    logo:{
        fontFamily: 'Great Vibes',
        fontSize:48,
        color: Colors.stylish.tomato,
    },
    field:{
        marginVertical:10,
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray
    },
    input:{
        paddingVertical:10,
        fontSize:15,
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular'
    },
    buttonWrapper:{
        marginTop:20,
        marginBottom:50,
    },
    buttonLogin:{
        borderWidth:1,
        borderColor: Colors.stylish.tomato,
        borderRadius:25,
        paddingVertical:10,
        marginHorizontal: width * 0.15,
    },
    buttonLoginTextWrapper:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonLoginText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        color: Colors.stylish.tomato,
    },
    buttonForgotPass:{
        marginTop:15,
        paddingVertical:10,
        alignItems: 'center'
    },
    buttonForgotPassTextWrapper:{

    },
    buttonForgotPassText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:12,
        color: Colors.stylish.tomato,
    },
    // Register Section 
    register:{
        flex:1,
    },
    buttonRegister:{
        flex:1,
        paddingBottom:20,
        borderTopWidth:1,
        borderColor: Colors.general.lightGray
    },
    buttonRegisterTextWrapper:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonRegisterText:{
        fontFamily: "Montserrat",
        fontSize: 18,
        fontWeight: "300",
        color: Colors.stylish.tomato
    }
})