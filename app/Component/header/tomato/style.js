import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";

// ------ Importing colors
import Colors from "../../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    header:{
        flex:1,
        backgroundColor: Colors.stylish.tomato,
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray,
    },
    logoWrapper:{
        flex:1,
        justifyContent: Platform.OS == 'ios' ? 'flex-end' : 'center',
        alignItems: 'flex-start',
        paddingLeft:20
    },
    logo:{
        //transform: [{ rotate: '-10deg' }],
        fontFamily: Platform.OS == 'ios' ? 'Great Vibes' : 'Great_Vibes-Regular',
        color: Colors.general.white,
        fontSize: Platform.OS == 'ios' ? 30 : 40
    },
    configButtonWrapper:{
        position: 'absolute',
        right: 0,
        bottom: Platform.OS == 'ios' ? 0 : 15,
    },
    configButton:{
        padding:5,
    },
    configButtonIconWrapper:{
        paddingRight:15,
    },
    configButtonIcon:{
        fontSize: 30, //Platform.OS == 'ios' ? 30 : 40,
        color: Colors.general.white
    },
    backButton:{
        position:'absolute',
        padding:5,
        paddingHorizontal:15,
        left:0,
        bottom: Platform.OS == 'ios' ? 0 : 18,
        zIndex:3,
    }
})