import React, { Component } from "react";
import {
    View,
    Text
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
// ------ Importing styles
import styles from "./style";
// ------ Importing Colors
import Colors from "../../Theme/colors";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={

        }
    }
    // React navigations props
    static navigationOptions= ({ navigation }) => {
        return {
            title: "Settings",
            tabBarIcon: ({ focused, tintColor }) => {
                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return <Icon name={'ios-settings'} size={25} color={tintColor} />;
            },
        }
    }

    render(){
        return(
            <View style={ styles.wrapper }>
                <Text style={ styles.comingSoon }>Coming Soon...</Text>
            </View>
        );
    }
}

export default Index;