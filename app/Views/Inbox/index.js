import React, { Component } from "react";
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    Image,
} from "react-native";
// ------ Importingh Component
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/Ionicons";
import Header from "../../Component/header/tomato";
// ------ Importing styles
import styles from "./style";
// ------ Importing Routes
import Routes from "../../Navigator/Routes";
// ------ Importing API helper
import API from "../../API";
// ------ Importing activity indicator actions
import ActivityIndicatorActions from "../../Service/Actions/ActivityIndicator";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            list: List,
            requests: [],
            activeMenu: 1,
            initFetchRequest: false,
            initFetchChat: false,
        }
    }


    // React navigations props
    static navigationOptions = ({ navigation }) => {
        return {
            title: "Inbox",
            tabBarIcon: ({ focused, tintColor }) => {
                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return <Icon name={'ios-mail'} size={25} color={tintColor} />;
            },
        }
    }

    _selectImage(num){
        switch(num){
            case 1: {
                return require("../../Resources/Image/1.jpeg");
            }
            case 2: {
                return require("../../Resources/Image/2.jpg");
            }
            case 3: {
                return require("../../Resources/Image/5.jpg");
            }
            case 4: {
                return require("../../Resources/Image/4.jpg");
            }
            default:{
                return require("../../Resources/Image/3.jpg")
            }
        }
    }

    _changeSection(index){
        if(index == 1){
            this.setState({ activeMenu: 1})
        } else {
            this.setState(
                { activeMenu: 2},
                ()=>{
                    this._fetchRequest()
                }
            )
        }
    }

    _fetchRequest(){
        if(!this.state.initFetchRequest){
            this.props.dispatch(dispatch => {
                dispatch({ type: ActivityIndicatorActions.ACTIVATE })
                API.GET(`/requests/${this.props.session.user.profile.uid}`)
                .then(resp => {
                    dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                    console.log("Requests: ", resp);
                    this.setState({ requests: resp })
                }).catch(err => {
                    dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                    console.log("Error: ", err);
                })
            })
        }
    }

    fetchChat(){
        if(!this.state.initFetchChat){
            console.log("Fetch chats");
        }
    }

    _mail(item,index){
        return(
            <TouchableOpacity
                key={ index }
                onPress={()=>{ 
                    this.props.navigation.navigate( 
                        Routes.Stack.Request, 
                        { 
                            id: item.id,
                            name: item.data.from_name,
                            uid: item.data.from,
                            refresh: () => { this._fetchRequest() } 
                        } 
                    )
                }}>
                <View style={ styles.mail}>
                    <View style={ styles.titleWrapper }>
                        <Text style={ styles.title }>{ item.data.title }</Text>
                    </View>
                    <View style={ styles.fromWrapper }>
                        <Text style={ styles.from }>From:</Text>
                        <Text style={ styles.whom}>{ item.data.from_name}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    _chat(item,index){
        return(
            <TouchableOpacity
                key={ index }
                onPress={()=>{ 
                    this.props.navigation.navigate( Routes.Stack.Chat )
                }}>
                <View style={ styles.chat }>
                    <View style={ styles.avatar}>
                        <View style={ styles.imageWrapper }>
                            <Image 
                                style={ styles.image}
                                source={ this._selectImage(index) }
                            />
                        </View>
                    </View>
                    <View style={ styles.chatInfo }>
                        <View style={ styles.nameWrapper }>
                            <Text style={ styles.name }>Name</Text>
                        </View>
                        <View style={ styles.lastMessageWrapper }>
                            <Text style={ styles.lastMessage }>New wave is coming.</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    _renderItem(item, index){
        if(this.state.activeMenu == 1){
            return this._chat(item,index);
        } else {
            return this._mail(item, index)
        }
    }

    _empty(){
        return(
            <View style={ styles.emptyWrapper }>
                <Text style={ styles.emptyText}>
                    {
                        this.state.activeMenu == 1 ?
                            "There is no chat history at the moment."
                            :
                            "There is no mail at the moment."
                    }
                </Text>
            </View>
        )
    }

    _renderList(){
        if(this.state.activeMenu == 1){
            if(this.state.list.length > 0){
                return(
                    <FlatList 
                        style={{ flex:1 }}
                        data={ this.state.list}
                        renderItem={({item, index}) => this._renderItem(item,index)}
                    />
                )
            } else {
                return this._empty()
            }
        } else {
            if(this.state.requests.length > 0){
                return(
                    <FlatList 
                        style={{ flex:1 }}
                        data={ this.state.requests }
                        renderItem={({item, index}) => this._renderItem(item,index)}
                    />
                )
            } else {
                return this._empty()
            }
        }
        
    }

    render(){
        return(
            <View style={ styles.wrapper}>
                <Header 
                    title={'Communicate'}
                    rightButton={{
                        onPress: ()=>{ alert('Enable Search!')},
                        name: "ios-search"
                    }}
                />
                <View style={ styles.contentWrapper }>
                    <View style={ styles.subHeader }>
                        <TouchableOpacity
                            style={[
                                styles.subMenu,
                                styles.subMenuLeft,
                                this.state.activeMenu == 1 ? styles.activeSubMenu : null
                            ]}
                            onPress={() => { this._changeSection(1) }}
                            >
                            <View>
                                <Text style={[ 
                                    styles.subMenuText,
                                    this.state.activeMenu == 1 ? styles.activeSubMenuText : null
                                    ]}>Chat</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[
                                styles.subMenu,
                                styles.subMenuRight,
                                this.state.activeMenu == 2 ? styles.activeSubMenu : null
                            ]}
                            onPress={() => { this._changeSection(2) }}
                            >
                            <View>
                                <Text style={[ 
                                    styles.subMenuText,
                                    this.state.activeMenu == 2 ? styles.activeSubMenuText : null
                                    ]}>Inbox</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={ styles.content }>
                        { this._renderList() }
                    </View>
                    
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    session: state.Session
})

export default connect(mapStateToProps)(Index);

const List = [
    {
        title: "Let's Collaborate",
        from: "Eva",
        date: "2018-10-13",
        message: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English."
    },
    {
        title: "Let's Collaborate",
        from: "Eva",
        date: "2018-10-13",
        message: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English."
    },
    {
        title: "Let's Collaborate",
        from: "Eva",
        date: "2018-10-13",
        message: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English."
    },
    {
        title: "Let's Collaborate",
        from: "Eva",
        date: "2018-10-13",
        message: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English."
    },
]