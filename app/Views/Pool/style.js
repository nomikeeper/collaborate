import {
    StyleSheet,
    Dimensions,
    Platform
} from "react-native";
// ------ Importing Colors
import Colors from "../../Theme/colors";

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    outerWrapper:{
        flex:1,
    },
    wrapper:{
        flex:1,
        backgroundColor: Colors.general.white,
    },
    headerWrapper:{
        flex:1,
        width:width,
    },
    buttonHeader:{
        flex:1,
        borderBottomWidth:1,
        borderColor: Colors.general.lightGray,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    buttonHeaderTextWrapper:{
        marginBottom:5,
    },
    buttonHeaderText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:20,
        color: Colors.stylish.tomato,
    },
    buttonOption:{
        position: 'absolute',
        padding:10,
    },
    buttonOptionIconWrapper:{
        flex:1,
        fontSize:20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonOptionIcon:{
        color: Colors.stylish.tomato
    },
    carouselWrapper:{
        flex: 9,
        paddingTop:height * 0.05
    },
    slide:{ 
        flexDirection: 'column',
        borderWidth: 1,
        borderColor: '#000',
        borderRadius:10,
        zIndex: 2,
    },
    imageWrapper:{
        width: width * 0.7 -2,
        height: height * 0.3,
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        overflow: 'hidden',
    },
    image:{
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        width: width * 0.7 -2,
        height: height * 0.3,
        resizeMode: 'cover',
    },
    description:{
        height: height * 0.4,
        width: width * 0.7,
        borderBottomLeftRadius:10,
        borderBottomRightRadius: 10,
        backgroundColor: "rgba(255,255,255,0.7)",
        padding:10,
    },
    name:{
        fontSize:18,
    },
    section:{
        flex:1,
    },
    labelWrapper:{
        alignItems:'flex-start'
    },
    labelBorder:{
        borderBottomWidth: 1,
        borderColor: Colors.general.dark
    },
    label:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontWeight: 'bold',
    },
    marginTop10:{
        marginTop:10,
    },
    descWrapper:{
        marginTop:10,
    },
    desc:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:12,
        lineHeight:16,
    },
    button:{
        flex:1,
        borderTopWidth: 1,
        borderColor: Colors.general.lightGray,
        justifyContent:'center',
        alignItems: 'center',
    },
    textWrapper:{

    },
    text:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        color: Colors.stylish.tomato,
    },
});

export const modal = StyleSheet.create({
    wrapper:{
        flex:1
    },
    content:{
        flex:1,
    },
    closeButton:{
        position: 'absolute',
        zIndex:5,
        padding:5,
        top:40,
        right:15,
    },
    filterWrapper:{
        flex:9,
        marginTop: Platform.OS == 'ios' ? 80 : 55,
        paddingHorizontal:20,
    },
    section:{
        flex:1,
        justifyContent: 'center',
        marginBottom:25,
        borderTopWidth: 1,
        borderColor: Colors.general.lightGray
    },
    buttonLogout:{
        borderWidth:1,
        borderColor: Colors.stylish.tomato,
        borderRadius:25,
        paddingVertical:10,
        marginHorizontal: width * 0.15,
    },
    buttonLogoutTextWrapper:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonLogoutText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:16,
        color: Colors.stylish.tomato,
    },
})