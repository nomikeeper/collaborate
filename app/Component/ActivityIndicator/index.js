import React, { Component } from "react";
import {
    View,
    ActivityIndicator,
} from "react-native";
// ------ Importing Styles
import styles from "./style";
// ------ Importing Colors
import Colors from "../../Theme/colors";

class Index extends Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        return(
            <View style={ styles.wrapper }>
                <ActivityIndicator 
                    style={ styles.indicator }
                    size="large"
                    color={ Colors.general.dark }
                />
            </View>
        )
    }
}

export default Index;