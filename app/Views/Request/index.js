import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    Alert,
    Platform
} from 'react-native';
import { connect } from "react-redux";
import { SafeAreaView } from 'react-navigation';
import Icon from "react-native-vector-icons/Ionicons";
// ------ Importing styles
import styles from "./style";
// ------ Importing Colors
import Colors from "../../Theme/colors";
// ------ Importing Routes
import Routes from "../../Navigator/Routes";
// ------ Importing API Helper
import API from "../../API";
// ------ Importing Activity Indicator Actions
import ActivityIndicatorActions from "../../Service/Actions/ActivityIndicator";

class Index extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            header: false,
            title: 'Mail',     
            headerTitleStyle:{
                fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
                color: "#FFF"
            },
            headerStyle: {
                backgroundColor: Colors.stylish.tomato,
            },
            headerLeft: <TouchableOpacity
                            style={ styles.backButton }
                            onPress={() => { navigation.goBack() }}
                            >
                            <View>
                                <Icon 
                                    size={ 30 }
                                    color={ '#FFF' }
                                    name={'ios-arrow-back'}
                                />
                            </View>
                        </TouchableOpacity>,
            headerRight: <TouchableOpacity
                            style={ styles.backButton }
                            onPress={() => { 
                                Alert.alert("Are you sure?",
                                            "Once you delete it, it cannot be restored.",
                                            [
                                                { text:'Cancel', onPress: () => { console.log('cancel')}, style: 'cancel' },
                                                { text:'Ok', onPress: () => { 
                                                        API.setHeader({id: navigation.state.params.id})
                                                        API.GET("/delete_request")
                                                        .then(resp => {
                                                            navigation.state.params.refresh()
                                                            navigation.goBack();
                                                        }).catch(err => {
                                                            console.log("Error: ", err)
                                                        })
                                                    }
                                                },
                                            ],
                                            { cancelable: true }
                                            ) 
                            }}
                            >
                            <View>
                                <Icon 
                                    size={ 30 }
                                    color={ '#FFF' }
                                    name={'md-trash'}
                                />
                            </View>
                        </TouchableOpacity>,
        }
    }

    render(){
        const { uid, name } = this.props.navigation.state.params
        return(
                <View style={ styles.wrapper}>
                    <View style={ styles.content }>
                        <View style={[ styles.section, { flexDirection: 'row' } ]}>
                            <Text style={ styles.from }>From: </Text>
                            <Text 
                                style={ styles.sender }
                                onPress={() => { 
                                    this.props.dispatch(dispatch => {
                                        dispatch({ type: ActivityIndicatorActions.ACTIVATE })
                                        fetch(`http://localhost:8080/profile_by_uid/${uid}`, {
                                            method: "GET"
                                        }).then(resp => resp.json())
                                        .then(resp => {
                                            dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                                            this.props.navigation.navigate(
                                                Routes.Stack.ProfileViewer, 
                                                { 
                                                    profile: resp , 
                                                    collaborate: false,
                                                    like: true, 
                                                }
                                            ) 
                                        }).catch(err => {
                                            dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                                            console.log("Error: ", err)
                                        })
                                        
                                    })
                                    }}>
                                { name }
                            </Text>
                        </View>
                        <View style={[ styles.section, { marginVertical:0} ]}>
                            <Text style={ styles.date }>2018-10-01 16:20</Text>
                        </View>
                        <View style={ styles.section }>
                            <View style={ styles.titleWrapper}>
                                <Text style={ styles.title }>Title will be here</Text>
                            </View>
                        </View>
                        <View style={ styles.section }>
                            <Text style={ styles.mailContext }>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque enim lectus, egestas eu ultricies id, gravida in lectus. Vestibulum porta tortor tellus, ut malesuada leo varius non.</Text>
                        </View>
                    </View>
                    <View style={ styles.control }>
                        <TouchableOpacity
                            style={[
                                styles.controlBtn
                            ]}
                            onPress={() => { alert("Rejected!") }}
                            >
                            <View>
                                <Text style={[ styles.controlBtnText ]}>Reject</Text>
                            </View>
                        </TouchableOpacity>
                        
                        <TouchableOpacity
                            style={[
                                styles.controlBtn
                            ]}
                            onPress={() => { alert("Accepted!") }}
                            >
                            <View>
                                <Text style={[ styles.controlBtnText ]}>Accept</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
        );
    }
}

const mapStateToProps = state => ({
    session: state.Session
})
export default connect(mapStateToProps)(Index);