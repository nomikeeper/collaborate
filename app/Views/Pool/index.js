import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Dimensions,
    TouchableOpacity,
    Modal,
    Alert
} from "react-native";
import { SafeAreaView } from "react-navigation";
import { connect } from "react-redux";
import Carousel from "react-native-snap-carousel";
import Icon from "react-native-vector-icons/Ionicons";
// ------ Importing styles
import styles, { modal } from "./style";
// ------ Importing Routes
import Routes from "../../Navigator/Routes";
// ------ Importing color
import Colors from "../../Theme/colors";
// ------ Importing API Helper
import API from "../../API";
// ------ Importing Activity indicator actions
import ActivityIndicatorActions from "../../Service/Actions/ActivityIndicator";

const { width, height } = Dimensions.get('window');

class Index extends Component{
    constructor(props){
        super(props);
        this.state={
            modalVisibility: false,
            entries:[
                { 
                    title: "1",
                    path: "../../Resources/Image/1.jpeg"
                },
                { 
                    title: "2",
                    path: "../../Resources/Image/2.jpg"
                },
                { 
                    title: "3",
                    path: "../../Resources/Image/4.jpg"
                },
                { 
                    title: "4",
                    path: "../../Resources/Image/5.jpg"
                },
            ],
            timer: null,
            pool: [],
        }
    }

    // React navigations props
    static navigationOptions= ({ navigation }) => {
        return {
            title: "Pool",
            tabBarIcon: ({ focused, tintColor }) => {
                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return <Icon name={'ios-albums'} size={25} color={tintColor} />;
            },
        }
    }

    componentWillMount(){
        this.props.dispatch(dispatch=> {
            dispatch({ type: ActivityIndicatorActions.ACTIVATE })
            API.GET("/pool")
                .then(resp => {
                    dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                    this.setState({ pool: resp })
                }).catch(err => {
                    dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                    console.log("Error: ", err)
                })
        })
        
    }

    _selectImage(num){
        switch(num){
            case 1: {
                return require("../../Resources/Image/1.jpeg");
            }
            case 2: {
                return require("../../Resources/Image/2.jpg");
            }
            case 3: {
                return require("../../Resources/Image/5.jpg");
            }
            case 4: {
                return require("../../Resources/Image/4.jpg");
            }
            default:{
                return require("../../Resources/Image/3.jpg")
            }
        }
    }

    _openModal(){
        this.setState({ modalVisibility: true })
    }

    _closeModal(){
        this.setState({ modalVisibility: false })
    }

    _renderItem ({item, index}) {
        return (
            <View key={index} style={styles.slide}>
                <View style={ styles.imageWrapper }>
                    <Image 
                        style={styles.image}
                        source={ this._selectImage(index) }
                    />
                </View>
                <View style={ styles.description }>
                    <View style={ styles.section }>
                        <Text style={[ styles.label, styles.name ]}>{ item.name }</Text>
                    </View>
                    <View style={[ styles.section, { flex: 3, justifyContent: 'center' } ]}>
                        <View style={ styles.labelWrapper}>
                            <View style={ styles.labelBorder }>
                                <Text style={ styles.label }>About me</Text>
                            </View>
                        </View>
                        <View style={ styles.descWrapper }>
                            <Text style={ styles.desc }>{ item.about_me }</Text>
                        </View>
                    </View>
                    <View style={[ styles.section, { flex: 2, justifyContent: 'center' } ]}>
                        <View style={ styles.labelWrapper}>
                            <View style={ styles.labelBorder }>
                                <Text style={ styles.label }>Interested In</Text>
                            </View>
                        </View>
                        <View style={ styles.descWrapper }>
                            <Text style={ styles.desc }>{ item.interested_in }</Text>
                        </View>
                    </View>
                    <View style={[ styles.section ]}>
                        <TouchableOpacity
                            style={ styles.button }
                            onPress={() => { 
                                this.props.navigation.navigate( Routes.Stack.ProfileViewer, 
                                    { 
                                        name: item.name,
                                        collaborate: true,
                                        like: true,
                                        profile: item,
                                        profile_pic: index
                                    }
                                )}
                            }>
                            <View style={ styles.textWrapper }>
                                <Text style={ styles.text }>View</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

    render(){
        return(
                    <View style={ styles.wrapper }>
                        <View style={ styles.headerWrapper}>
                            <TouchableOpacity
                                onPress={() => { 
                                    this.state.timer ?
                                        alert("You can't refresh right now.")
                                        :
                                        Alert.alert(
                                            "Attention",
                                            "It will refresh the pool",
                                            [
                                                { text: 'Ok', onPress:() => { }}
                                            ]
                                        ) 
                                }}
                                style={[ styles.buttonHeader ]}
                                >
                                <View style={ styles.buttonHeaderTextWrapper }>
                                    <Text style={ styles.buttonHeaderText }>
                                        {
                                            this.state.timer ?
                                                this.state.timer
                                                :
                                                'Refresh'
                                        }
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={ styles.carouselWrapper }>
                            <TouchableOpacity 
                                style={[ styles.buttonOption, { right: 10 } ]}
                                onPress={() => { this.props.navigation.navigate( Routes.Stack.Favorite) }}
                                >
                                <View style={ styles.buttonOptionIconWrapper }>
                                    <Icon 
                                        style={styles.buttonOptionIcon}
                                        name={'ios-images'}
                                        size={30}
                                    />
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                style={[ styles.buttonOption, { left: 10 } ]}
                                onPress={() => { this._openModal() }}
                                >
                                <View style={ styles.buttonOptionIconWrapper }>
                                    <Icon 
                                        style={styles.buttonOptionIcon}
                                        name={'ios-settings'}
                                        size={30}
                                    />
                                </View>
                            </TouchableOpacity>
                            <Carousel
                                ref={(c) => { this._carousel = c; }}
                                data={this.state.pool}
                                renderItem={this._renderItem.bind(this)}
                                sliderWidth={ width }
                                sliderHeight={ height }
                                itemWidth={ width * 0.7 }
                                itemHeight={ height * 0.7 }
                            />
                        </View>

                        <Modal
                            style={ modal.wrapper }
                            visible={ this.state.modalVisibility}
                            animationType={'slide'}
                            >
                            <View style={ modal.content }>
                                <TouchableOpacity
                                    style={ modal.closeButton }
                                    onPress={()=>{ this._closeModal()}}
                                    >
                                    <View>
                                        <Icon 
                                            size={ 40 }
                                            name="ios-close-circle-outline"
                                            color={ Colors.stylish.tomato }
                                        />
                                    </View>
                                </TouchableOpacity>
                                <View style={ modal.filterWrapper }>
                                    <Text>Filters will be listed here</Text>
                                </View>
                                <View style={ modal.section }>
                                    <TouchableOpacity
                                        style={ modal.buttonLogout}
                                        onPress={() => { 
                                            this._closeModal()
                                        }}
                                        >
                                        <View style={ modal.buttonLogoutTextWrapper }>
                                            <Text style={ modal.buttonLogoutText }>Apply filter</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                    </View>
        );
    }
}

const mapStateToProps = state => ({
    session: state.Session
})
export default connect(mapStateToProps)(Index);