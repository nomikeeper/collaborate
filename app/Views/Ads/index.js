import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    Modal,
} from "react-native";
import { SafeAreaView } from 'react-navigation';
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/Ionicons";
import MdIcon from "react-native-vector-icons/MaterialCommunityIcons";
import moment from "moment";
// ------ importing custom components
import Header from "../../Component/header/tomato";
// ------ Importing styles
import styles, { modal } from "./style";
// ------ Importing Colors
import Colors from "../../Theme/colors";
import Routes from '../../Navigator/Routes';
// ------ Importing API helper
import API from "../../API";
// ------ Importing activityIndicatorActons
import ActivityIndicatorActons from "../../Service/Actions/ActivityIndicator"

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            modalVisibility: false,
            initialFetch: false,
            ads: [],
            myAds: [],
            isMyAdsActive: false,
            isMyAdsFetched: false,
        }
    }

    // React navigations props
    static navigationOptions= ({ navigation }) => {
        return {
            title: "Events",
            tabBarIcon: ({ focused, tintColor }) => {
                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return <Icon name={'ios-list'} size={25} color={tintColor} />;
            },
        }
    }

    componentWillMount(){
        if(!this.state.initialFetch){
            this.props.dispatch(dispatch => {
                dispatch({ type: ActivityIndicatorActons.ACTIVATE })
                API.setHeader({
                    "profile_id": this.props.session.user.profile_id
                })
                API.GET("/ads")
                .then(resp => {
                    if(resp){
                        this.setState({
                            ads: resp
                        }, () => {
                            dispatch({ type: ActivityIndicatorActons.DEACTIVATE })
                        })
                    }
                }).catch(err => {
                    dispatch({ type: ActivityIndicatorActons.DEACTIVATE })
                    console.log("Error: ", err);
                })
            })
        }
    }

    _getMyAds(){
            // Fetch My ads
            this.props.dispatch(dispatch => {

                API.setHeader({
                    "profile_id": this.props.session.user.profile_id
                })
                API.GET('/my_ads')
                .then(resp => {
                    if(resp){
                        this.setState({
                            myAds: resp,
                            isMyAdsFetched: this.state.isMyAdsFetched == false ? true : this.state.isMyAdsFetched
                        }, () => {
                            dispatch({ type: ActivityIndicatorActons.DEACTIVATE })
                        })
                    }
                }).catch(err => {
                    dispatch({ type: ActivityIndicatorActons.DEACTIVATE })
                    console.log("Error: ", err);
                })
            })
    }



    _refetchAds(){
        if(this.state.isMyAdsActive){
            this._getMyAds()
        } else {
            this.props.dispatch(dispatch => {
                dispatch({ type: ActivityIndicatorActons.ACTIVATE })
                API.GET("/ads")
                .then(resp => {
                    console.log("All ads: ", resp)
                    if(resp){
                        this.setState({
                            ads: resp
                        }, () => {
                            dispatch({ type: ActivityIndicatorActons.DEACTIVATE })
                        })
                    }
                }).catch(err => {
                    dispatch({ type: ActivityIndicatorActons.DEACTIVATE })
                    console.log("Error: ", err);
                })
            });
        }
    }

    _openModal(){
        this.setState({ 
            modalVisibility: true
        })
    }

    _closeModal(){
        this.setState({ 
            modalVisibility: false
        })
    }

    _renderItem(item, index){
        return(
            <TouchableOpacity 
                key={ index }
                onPress={() => { 
                    this.props.navigation.navigate(
                        Routes.Stack.Ad,
                        { 
                            ad: item,
                            refresh: () => { this._refetchAds() } 
                        }
                    ) 
                }}
                style={ styles.item }>
                <View style={ styles.itemContentWrapper}>
                    <View style={ styles.itemContent }>
                        <View style={ styles.titleWrapper }>
                            <Text style={ styles.title }>{item.data.title}</Text>
                        </View>
                        <View style={ styles.categoryWrapper }>
                            <Text style={ styles.categoryLabel}>Category:</Text>
                            <Text style={ styles.category }>{ item.data.category }</Text> 
                        </View>
                        <View style={ styles.deadlineWrapper}>
                            <Text style={ styles.deadlineLabel} >Deadline: </Text>
                            <Text style={ styles.deadline}>{moment(item.data.deadline).format("YYYY-MM-DD")}</Text>
                        </View>
                    </View>
                    <View style={ styles.itemIconWrapper}>
                        <Icon 
                            style={ styles.itemIcon}
                            name={'logo-youtube'}
                            size={25}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    // Activate or Deactivate My ads
    _switchMyAdsState(){
        this.props.dispatch(dispatch => {
            dispatch({ type: ActivityIndicatorActons.ACTIVATE })
            if(this.state.isMyAdsFetched){
                setTimeout(() => {
                    this.setState({ isMyAdsActive: !this.state.isMyAdsActive },
                        () => {
                            dispatch({ type: ActivityIndicatorActons.DEACTIVATE })

                        })
                }, 300)
            } else {
                if(!this.state.isMyAdsFetched){
                    this._getMyAds()
                }
                setTimeout(() => {
                    this.setState({ isMyAdsActive: !this.state.isMyAdsActive },
                        () => {
                            dispatch({ type: ActivityIndicatorActons.DEACTIVATE })

                        })
                }, 300)
            }
            
        })
    }

    // When ads or my ads empty
    _empty(){
        return(
            <View style={ styles.emptyWrapper }>
                <Text style={ styles.emptyText}>Currently, there is no Ad to show.</Text>
            </View>
        )
    }
    // Show the list of the ads
    _ads(){
        if(this.state.isMyAdsActive){
            if(this.state.myAds.length > 0){
                return (<FlatList 
                    style={{ flex: 1}}
                    data={this.state.myAds}
                    renderItem={({item, index}) => this._renderItem(item, index)}
                />)
            } else {
                return this._empty()
            }
        } else {
            if(this.state.ads.length > 0){
                return (<FlatList 
                    style={{ flex: 1}}
                    data={this.state.ads}
                    renderItem={({item, index}) => this._renderItem(item, index)}
                />)
            } else {
                return this._empty()
            }
        }
    }

    render(){
        return(
            <View style={ styles.wrapper }>
                <Header 
                    title={'Collaborate'}
                    titleButton={
                        <TouchableOpacity
                            style={[ 
                                styles.headerTitleBtn,
                                this.state.isMyAdsActive ?
                                { backgroundColor: Colors.general.white}
                                :
                                { backgroundColor: 'transparent' }
                            ]}
                            onPress={() => { this._switchMyAdsState() }}
                            >
                            <View>
                                <Text style={[
                                    styles.headerTitleBtnText,
                                    this.state.isMyAdsActive ?
                                    { color: Colors.stylish.tomato}
                                    :
                                    { color: Colors.general.white }
                                ]}>My Ads</Text>
                            </View>
                        </TouchableOpacity>
                    }
                    isTitleButton={true}
                    rightButton={{
                        onPress: () => { this._openModal() },
                        name: 'ios-settings'
                    }}
                />
                <View style={ styles.content}>
                    { this._ads() }
                </View>
                <TouchableOpacity
                    style={ styles.addEventBtn }
                    onPress={() => { 
                        this.props.navigation.navigate( 
                            Routes.Stack.CreateAd, 
                            { refetchAds: () => this._refetchAds() } 
                        ) 
                    }}
                    >
                    <View style={ styles.addEventIconWrapper }>
                        <MdIcon
                            style={ styles.addEventIcon } 
                            size={30}
                            color={ "#FFF" }
                            name={'pencil'}
                        />
                    </View>
                </TouchableOpacity>
                <Modal
                    style={ modal.wrapper }
                    visible={ this.state.modalVisibility}
                    animationType={'slide'}
                    >
                    <View style={ modal.content }>
                        <TouchableOpacity
                            style={ modal.closeButton }
                            onPress={()=>{ this._closeModal()}}
                            >
                            <View>
                                <Icon 
                                    size={ 40 }
                                    name="ios-close-circle-outline"
                                    color={ Colors.stylish.tomato }
                                />
                            </View>
                        </TouchableOpacity>
                        <View style={ modal.filterWrapper }>
                            <Text>Filters will be listed here</Text>
                        </View>
                        <View style={ modal.section }>
                            <TouchableOpacity
                                style={ modal.buttonLogout}
                                onPress={() => { 
                                    this._closeModal()
                                }}
                                >
                                <View style={ modal.buttonLogoutTextWrapper }>
                                    <Text style={ modal.buttonLogoutText }>Apply filter</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const mapPropsToState = state => ({
    session: state.Session
})
export default connect(mapPropsToState)(Index);

const Data = [
    {
        title: "Looking for collaboration",
        category: "Gaming",
        deadline: "2018-08-05"
    },
    {
        title: "Looking for actor",
        category: "Youtube video",
        deadline: "2018-08-10"
    },
    {
        title: "Looking for actress",
        category: "Short film",
        deadline: "2018-09-15"
    },
    {
        title: "Random Stuff",
        category: "Talking",
        deadline: "2018-10-05"
    },
    {
        title: "Let's sing, write, hang out",
        category: "Songwriting",
        deadline: "2018-09-22"
    },
]