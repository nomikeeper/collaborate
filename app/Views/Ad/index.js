import React, { Component } from "react";
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Platform,
    Dimensions,
    Alert
} from "react-native";
import moment from "moment";
import { connect } from "react-redux";
// ------ Importing custom components
import Header from "../../Component/header/tomato";
import GenericButton from "../../Component/GenericButton"
// ------ Importing styles
import styles from "./style";
import Routes from "../../Navigator/Routes";
// ------ Importing Colors
import Colors from "../../Theme/colors";
import ActivityIndicatorActions from "../../Service/Actions/ActivityIndicator";

const { width, height } = Dimensions.get('window');

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            ad: null,
        }
    }

    componentWillMount(){
        const { ad } = this.props.navigation.state.params
        if(!this.state.ad && ad){
            // console.log("Advertisment: ", ad)
            // console.log("This.props: ", this.props.session)
            this.setState({ ad: ad })
        }
    }

    _delete(){
        Alert.alert(
            'Are you sure?',
            'Once the advertisement deleted, it cannot be restored.',
            [
                { text: 'Cancel', type: 'cancel'},
                { text: 'Ok', onPress: () => { 
                    console.log("State: ", this.state)
                    this.props.dispatch(dispatch => {
                        dispatch({ type: ActivityIndicatorActions.ACTIVATE })
                        fetch("http://localhost:8080/delete_ad",{
                            method: 'POST',
                            headers:{
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify({id: this.state.ad.ad_id})
                        }).then(resp => resp.json())
                        .then(resp => {
                            console.log("Response: ", resp)
                            dispatch({ type: ActivityIndicatorActions.DEACTIVATE})
                            this.props.navigation.state.params.refresh();
                            this.props.navigation.goBack()
                        }).catch(err => {
                            console.log("Error: ", err)
                            dispatch({ type: ActivityIndicatorActions.DEACTIVATE})
                        })
                    })
                 }},
            ],
            {cancelable: true}
        )
    }

    _updateAd(ad){
        console.log("Ad: ", ad)
        // this.setState({ ad: ad })
    }
    _owner(){
        console.log("Owner   ID: ", this.state.ad.data.owner)
        console.log("Profile ID: ", this.props.session.user.profile_id)
        if(this.state.ad.data.owner == this.props.session.user.profile_id){
            return (
                <View style={{
                    width: width - 40,
                }}>
                    <GenericButton 
                        onPress={() => { 
                            this.props.navigation.navigate(
                                Routes.Stack.CreateAd, 
                                { 
                                    ad: this.state.ad, 
                                    edit: true,
                                    updateAd: (ad) => { this._updateAd(ad)}
                                }
                            )}
                        }
                        text={'Edit'}
                    />
                    <GenericButton 
                        style={ styles.marginTop15 }
                        onPress={() => { this._delete() }}
                        text={'Delete'}
                    />
                </View>
            )
        } else {
            return(
                <View style={{
                    width: width - 40,
                }}>
                    <TouchableOpacity 
                        style={ styles.ownerButton }
                        onPress={() => {
                            this.props.navigation.navigate(Routes.Stack.ProfileViewer, 
                                { 
                                    id: this.state.ad.data.owner,
                                    like: true, 
                                }
                            ) 
                        }}
                        > 
                        <View>
                            <Text style={ styles.ownerText }>Owner</Text>
                        </View>
                    </TouchableOpacity>
                    
                    <GenericButton 
                        style={styles.marginTop15}
                        onPress={ () => { alert('Send collab request!')}}
                        text={'Collaborate'}
                    />
                </View>
            )
        }
    }
    render(){
        return(
            <View style={ styles.wrapper }>
                <Header
                    title={ this.state.ad.data.title }
                    titleWrapperStyle={{
                        alignItems:'center',
                        paddingLeft:0,
                    }}
                    titleStyle={{
                        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
                        fontSize:18,
                        marginBottom:10,
                    }}
                    leftButton={true}
                    onPress={() => { this.props.navigation.goBack()}} 
                />
                <View style={ styles.content }>
                    <ScrollView style={ styles.scrollView }>
                        <View style={ styles.section }>
                            <View style={ styles.labelWrapper }>
                                <Text style={ styles.label }>Info</Text>
                            </View>
                            <View style={ styles.infoWrapper }>
                                <Text style={ styles.infoText}>
                                    {this.state.ad.data.info }
                                </Text>
                            </View>
                        </View>
                        <View style={ styles.section }>
                            <View style={[ styles.categoryWrapper, styles.marginTop25, styles.borderTop ]}>
                                <Text style={ styles.categoryLabel }> Category : </Text>
                                <Text style={ styles.categoryText }> { this.state.ad.data.category }</Text>
                            </View>
                        </View>
                        <View style={ styles.section }>
                            <View style={ styles.deadlineWrapper }>
                                <Text style={ styles.deadlineLabel }> Deadline : </Text>
                                <Text style={ styles.deadlineText }> { moment(this.state.ad.data.deadline).format("YYYY-MM-DD") }</Text>
                            </View>
                        </View>
                        <View style={ styles.section}>
                        <View style={ styles.ownerWrapper }>
                            { /* <Text style={ styles.ownerLabel }> Owner : </Text> */ }
                            { this._owner() }
                        </View>
                    </View>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => ({
    session: state.Session
})

export default connect(mapStateToProps)(Index);