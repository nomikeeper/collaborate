import React, { Component } from 'react';
import {
    KeyboardAvoidingView,
    ScrollView,
    View,
    Text,
    TextInput,
    TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/Ionicons";
import DropdownAlert from "react-native-dropdownalert";
import KeyboardSpacer from "react-native-keyboard-spacer";
// ------ Importing Firebase
import  Firebase from "react-native-firebase";
// ------ Import Routes
import Routes from "../../Navigator/Routes";
// ------ Importing styles
import styles from "./style";
// ------ Importing API helper
import API from "../../API";
// ------ Activity Indicator Actions
import ActivityIndicatorActions from "../../Service/Actions/ActivityIndicator";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            email: '',
            password: '',
            passwordConfirm: '',
            statusText: '',
        }
    }

    // Validates the email
    _validateEmail(email){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    // Validates the user inputs
    _validate(){
        if(this._validateEmail(this.state.email)){
            if(this.state.password && this.state.password === this.state.passwordConfirm){
                return true;
            }
            else {
                this.setState({
                    statusText: 'Invalid Passwords.'
                }, () => {
                    return false;
                })
            }
        } else {
            this.setState({
                statusText: "Invalid Email."
            }, () => {
                return false;
            })
        }
    }
    _createAccount(){
        if( this._validate() ){
            let new_user = {
                email: this.state.email,
                password: this.state.password
            }
            this.props.dispatch(dispatch =>{
                dispatch({ type: ActivityIndicatorActions.ACTIVATE })
                API.POST("/create_user", JSON.stringify(new_user))
                .then(resp => {
                    dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                    console.log("New user: ", resp)
                    this.props.navigation.goBack();
                }).catch(err => {
                    dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                    console.log("Error: ", err);
                })
            })
            
        } else {
            this.onError(this.state.statusText);
        }
    }

    _update(field, text){
        switch(field){
            case 'email':{
                this.setState({
                    email: text
                })
                break;
            }
            case 'password':{
                this.setState({
                    password: text
                })
                break;
            }
            case 'passwordConfirm':{
                this.setState({
                    passwordConfirm: text
                })
                break;
            }
            case 'clear':{
                this.setState({
                    email: '',
                    password: '',
                    passwordConfirm: '',
                })
                break;
            }
            default :{
                return;
            }
        }
    }

    // ...
    onError = error => {
        if (error) {
        this.dropdown.alertWithType('error', 'Error', error);
        }
    };
    // ...
    onSuccess = success => {
        if (success) {
        this.dropdown.alertWithType('success', 'Success', success);
        }
    };
    // ...
    onClose(data) {
        console.log("Data: ", data)
        if(data.type == 'success'){
            this.props.navigation.goBack()
        }
        // data = {type, title, message, action}
        // action means how the alert was closed.
        // returns: automatic, programmatic, tap, pan or cancel
    }

    render(){
        return(
                    <View style={ styles.wrapper }>
                        <View style={ styles.registerHeader }>
                            <TouchableOpacity
                                style={ styles.backButton }
                                onPress={() => { this.props.navigation.goBack() }}
                                >
                                <View style={ styles.backButtonIconWrapper }>
                                    <Icon 
                                        style={ styles.backButtonIcon }
                                        name={'ios-arrow-back'}
                                    />
                                </View>
                            </TouchableOpacity>
                            <Text style={ styles.registerHeaderTitle }>Registration</Text>
                        </View>
                        <View style={ styles.form }>
                            <KeyboardAvoidingView style={{ flex: 1 }}>
                                <ScrollView style={{ flex: 1,}}>
                                    <View style={styles.field}>
                                        <TextInput
                                            autoCapitalize={'none'}
                                            value={ this.state.email }
                                            clearTextOnFocus={true}
                                            clearButtonMode={'while-editing'}
                                            placeholder={'E-mail'} 
                                            style={ styles.input }
                                            onChangeText={(text) => { this._update('email', text)}}
                                        />
                                    </View>
                                    <View style={styles.field}>
                                        <TextInput
                                            autoCapitalize={'none'}
                                            value={ this.state.password}
                                            secureTextEntry={true}
                                            clearTextOnFocus={true}
                                            clearButtonMode={'while-editing'}
                                            placeholder={'Password'} 
                                            style={ styles.input }
                                            onChangeText={(text) => { this._update('password', text)}}
                                        />
                                    </View>
                                    <View style={styles.field}>
                                        <TextInput
                                            autoCapitalize={'none'}
                                            value={ this.state.passwordConfirm}
                                            secureTextEntry={true}
                                            clearTextOnFocus={true}
                                            clearButtonMode={'while-editing'}
                                            placeholder={'Password Confirmation'} 
                                            style={ styles.input }
                                            onChangeText={(text) => { this._update('passwordConfirm', text)}}
                                        />
                                    </View>
                                    <View style={ styles.buttonWrapper }>
                                        <TouchableOpacity
                                            style={ styles.buttonLogin }
                                            onPress={()=>{ this._createAccount() }}
                                            >
                                            <View style={ styles.buttonLoginTextWrapper }>
                                                <Text style={ styles.buttonLoginText }>Create Account</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </ScrollView>
                            </KeyboardAvoidingView>

                        </View>
                        {/* !!! Make sure it's the last component in your document tree. */ }
                        <DropdownAlert ref={ref => this.dropdown = ref} onClose={data => this.onClose(data)}/>
                    </View>
        )
    }
}

const mapStateToProps = state => ({
    session: state.session
})

export default connect(mapStateToProps)(Index);