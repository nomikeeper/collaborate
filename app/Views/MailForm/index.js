import React, { Component } from "react";
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Platform,
    Alert
} from "react-native";
import { connect } from 'react-redux';
// ------ Importing components
import Icon from "react-native-vector-icons/Ionicons";
// ------ Importing Custom components
import GenericButton from "../../Component/GenericButton";
// ------ Importing styles
import styles from "./style";
// ------ Importing Colors
import Colors from '../../Theme/colors';
// ------ Importing API helper
import API from "../../API";
// ------ Importing ActivityIndicatorActions
import ActivityIndicatorActions from "../../Service/Actions/ActivityIndicator";

class Index extends Component{
    constructor(props){
        super(props);
        this.state={
            title: '',
            info: '',
        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        return {
            header: false,
            title: 'Collaboration Request',     
            headerTitleStyle:{
                fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
                color: "#FFF"
            },
            headerStyle: {
                backgroundColor: Colors.stylish.tomato,
            },
            headerLeft: <TouchableOpacity
                            style={ styles.backButton }
                            onPress={() => { navigation.goBack() }}
                            >
                            <View>
                                <Icon 
                                    size={ 30 }
                                    color={ '#FFF' }
                                    name={'ios-arrow-back'}
                                />
                            </View>
                        </TouchableOpacity>
        }
    }


    _update(field, value){
        switch(field){
            case fields.title: {
                this.setState({ title: value});
                break;
            }
            case fields.info:{
                this.setState({ info: value });
                break;
            }
            default: {
                return;
            }
        }
    }

    // Send request
    _sendRequest(){
        const receiver = this.props.navigation.state.params.profile
        const sender = this.props.session.user.profile
        //console.log("Receiver Profile: ", receiver)
        //console.log("Sender Profile: ", sender)
        const data = {
            from: '',
            from_name: '',
            to: '',
            title: '',
            info: '',
        }
        data.from = sender.uid;
        data.from_name = sender.name;
        data.to = receiver.uid;
        data.title = this.state.title;
        data.info = this.state.info;
        console.log("Data: ", data)
        if(data.title && data.info){
            this.props.dispatch(dispatch => {
                dispatch({ type: ActivityIndicatorActions.ACTIVATE })
                API.POST("/create_request", JSON.stringify(data))
                .then(resp => {
                    console.log("Response:" , resp)
                    dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                    Alert.alert(
                        "Successfull",
                        "Request succesfully sent.",
                        [
                            {
                                text: "OK", onPress: () => { 
                                    this.props.navigation.goBack()
                                }
                            }
                        ]
                    );
                }).catch(err => {
                    dispatch({ type: ActivityIndicatorActions.DEACTIVATE })
                    console.log("Error: ", err)
                })
            })
            
        } else {
            Alert.alert(
                "Warning",
                "Please fill out the blanks!"
            )
        }
    }

    render(){
        return(
            <View style={ styles.wrapper }>
                <View style={ styles.field }>
                    <View style={ styles.labelWrapper }>
                        <View style={ styles.labelBorder }>
                            <Text style={ styles.label }>Title</Text>
                        </View>
                    </View>
                    <View style={ styles.inputWrapper }>
                        <TextInput 
                            clearButtonMode={'while-editing'}
                            numberOfLines={1}
                            placeholder={ "Title" }
                            maxLength={ 60 }
                            style={ styles.input }
                            value={ this.state.title}
                            onChangeText={(value) => { this._update(fields.title, value )}}
                        />
                    </View>
                </View>
                <View style={ styles.field }>
                    <View style={ styles.labelWrapper }>
                        <View style={ styles.labelBorder }>
                            <Text style={ styles.label }>Info</Text>
                        </View>
                    </View>
                    <View style={ styles.inputWrapper }>
                        <TextInput 
                            clearButtonMode={'while-editing'}
                            multiline={true}
                            placeholder={ "Info" }
                            maxLength={ 250 }
                            style={[ styles.input, styles.infoInput ]}
                            value={ this.state.info}
                            onChangeText={(value) => { this._update(fields.info, value )}}
                        />
                    </View>
                </View>
                <View>
                    <GenericButton 
                        onPress={() => { this._sendRequest() }}
                        text={ 'Send Request'}
                    />
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => ({
    session: state.Session
})

export default connect(mapStateToProps)(Index);

const fields = {
    title: 'title',
    info: 'info',
}