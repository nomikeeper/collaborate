import {
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';
// ------ Importing Colors
import Colors from '../../Theme/colors';

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper:{
        flex: 1,
        backgroundColor: Colors.general.white
    },
    content:{
        flex:9,
    },
    scrollView:{
        flex:1,
        paddingVertical:25,
        paddingHorizontal: 20,
    },
    section:{

    },
    labelWrapper:{
        borderBottomWidth:1,
        borderColor: Colors.general.dark,
        paddingVertical: 5,
        alignSelf: 'flex-start',
    },
    label:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:18,
    },
    infoWrapper:{
        marginTop:15,
    },
    infoText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontSize:14,
        lineHeight:20,
    },
    borderTop:{
        borderTopWidth:1,
        borderColor: Colors.general.lightGray,
    },
    categoryWrapper:{
        paddingTop: 15,
        flexDirection: 'row',
    },
    categoryLabel:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontWeight: '500',
        color: Colors.stylish.tomato,
    },
    categoryText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontStyle: 'italic'
    },
    deadlineWrapper:{
        paddingTop:15,
        flexDirection: 'row',
    },
    deadlineLabel:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontWeight: '500',
        color: Colors.stylish.tomato,
    },
    deadlineText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontStyle: 'italic'
    },
    ownerWrapper:{
        paddingTop:15,
        flexDirection: 'row',
    },
    ownerLabel:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontWeight: '500',
        color: Colors.stylish.tomato,
    },
    ownerButton:{
        alignSelf:'flex-start',
        marginHorizontal: 5,
        paddingHorizontal:5,
        borderBottomWidth: 1,
        borderColor: Colors.general.dark
    },
    ownerText:{
        fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
        fontStyle: 'italic'
    },
    marginTop15:{
        marginTop:15,
    },
    marginTop25:{
        marginTop:25,
    }
})
