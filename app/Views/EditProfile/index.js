import React, { Component } from 'react';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Modal,
    Alert,
    Platform
} from 'react-native';
import { SafeAreaView } from "react-navigation";
import { connect } from "react-redux";
import { sessionService } from 'redux-react-native-session';
import moment from "moment";
// ------ Importing components
import Icon from "react-native-vector-icons/Ionicons";
// ------ Importing Custom components
import GenericButton from "../../Component/GenericButton";
// ------ Importing styles
import styles, { modal } from "./style";
// ------ Importinc Colors
import Colors from "../../Theme/colors";
// ------ Importing API helper
import API from "../../API";
// ------ Importing Icon types
import iconTypes from "../../_const/iconTypes";

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            modalVisibility: false,
            title: '',
            name: '',
            aboutMe: '',
            interestedIn: '',
            links: [],
            newPlatformName: '',
            newPlatformNameLink: '',
            newPlatformIcon: '',
            initialSet: false,
            setIconPopUp: false,
        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        return {
            header: false,
            title: params.name && params ? params.name : 'No name',     
            headerTitleStyle:{
                fontFamily: Platform.OS == 'ios' ? 'Montserrat' : 'Montserrat-Regular',
                color: "#FFF"
            },
            headerStyle: {
                backgroundColor: Colors.stylish.tomato,
            },
            headerLeft: <TouchableOpacity
                            style={ styles.backButton }
                            onPress={() => { navigation.goBack() }}
                            >
                            <View>
                                <Icon 
                                    size={ 30 }
                                    color={ '#FFF' }
                                    name={'ios-arrow-back'}
                                />
                            </View>
                        </TouchableOpacity>
        }
    }

    componentWillMount(){
        if(!this.state.initialSet){
            API.setHeader({    
                'Accept': 'application/json',
                "Content-Type": "application/json",
            })
            //console.log("Session: ", this.props.session)
            const { profile, user } = this.props.session.user
            //console.log("Profile: ", profile)
            //console.log("User: ", user)
            this.setState({
                title: profile.title,
                name: profile.name,
                aboutMe: profile.about_me,
                interestedIn: profile.interested_in,
                links: profile.links,
            },
            ()=> {
                //console.log("New state: ", this.state);
            })
        }
    }

    // Add link
    _addLink(){
        let newLink = {
            name: this.state.newPlatformName.toLowerCase(),
            link: this.state.newPlatformNameLink.toLowerCase(),
            icon: this.state.newPlatformIcon.toLowerCase(),
        }

        let newLinks = Object.assign([], this.state.links)
        newLinks[newLinks.length] = newLink;
        this.setState({
             links: newLinks,
             newPlatformIcon: '',
             newPlatformName: '',
             newPlatformNameLink: '',
        },
        () => {
            this._closeModal()
        }
        )
    }
    // Remove Link
    _removeLink(index){
        let newLinks = Object.assign([], this.state.links);
        newLinks.splice(index,1);
        this.setState({ links: newLinks })
    }
    // --- Update Field
    _update(field, value){
        switch(field){
            case fields.title : {
                this.setState({ title: value });
                break;
            }
            case fields.name : {
                this.setState({ name: value });
                break;
            }
            case fields.aboutMe: {
                this.setState({ aboutMe: value });
                break;
            }
            case fields.interestedIn: {
                this.setState({ interestedIn: value });
                break;
            }
            case fields.newPlatformName: {
                this.setState({ newPlatformName: value});
                break;
            }
            case fields.newPlatformNameLink:{
                this.setState({ newPlatformNameLink: value});
                break;
            }
            case fields.newPlatformIcon:{
                this.setState({ 
                    newPlatformIcon: value, 
                    setIconPopUp: false
                });
            }
            default: return;
        }
    }

    _openModal(){
        this.setState({ modalVisibility: true })
    }
    _closeModal(){
        this.setState({ modalVisibility: false })
    }

    _renderLink(item, index){
        return(
            <View style={ styles.platformBtn } key={index}>
                <View style={ styles.platformBtnContent }>
                    <Icon 
                        style={ styles.platformIcon }
                        name={`logo-${item.icon}`}
                    />
                    <Text style={ styles.platformUsername }>{ item.name }</Text>
                    <Icon 
                        onPress={() => { 
                            this._removeLink(index)
                        }}
                        style={[ 
                            styles.platformIcon,
                            styles.platformCloseBtn
                        ]}
                        name={'ios-close'}
                    />
                </View>
            </View>
        )
    }

    _renderLinksList(){
        if(this.state.links.length > 0){
            return this.state.links.map((item, index) => {
                return this._renderLink(item,index);    
            })
        } else {
            return <Text style={ styles.description }>No Links at the moment.</Text>
        }
    }

    _updateProfileConfirm(){

        let updatedProfile = this.props.session.user.profile;
        updatedProfile.title = this.state.title;
        updatedProfile.name = this.state.name;
        updatedProfile.interested_in = this.state.interestedIn;
        updatedProfile.links = this.state.links;
        updatedProfile.updated_at = moment().toISOString(); 
        let updateData = {
            profile: updatedProfile,
            profile_id: this.props.session.user.profile_id
        }

        //console.log("Update Values: ", updateData);
        Alert.alert(
            "Are you sure?",
            "You are about to update your profile information.",
            [
                {
                    text: "Cancel", onPress: () => { }, style: 'cancel'
                },
                {
                    text: "Ok", onPress: () => { this._updateProfile(updateData) }
                }
            ],
            { cancelable: true }
        )
    }

    _updateProfile(updatedProfile){
        API.PATCH("/update_profile", JSON.stringify(updatedProfile))
        .then(resp => {
            // Updating data of the local profile
            let _updatedUser = this.props.session.user
            _updatedUser.profile = resp;
            sessionService.saveUser(_updatedUser);
            sessionService.refreshFromAsyncStorage;
            this.props.navigation.goBack()
        }).catch( err => {
            console.log("Error: ", err)
        })
    }

    _confirm(title, context){
        Alert.alert(
            title,
            context,
            [
                {text: 'Cancel', onPress: () => { console.log('Cancel!')}, style: 'cancel'},
                {text: 'Ok', onPress: () => { this.props.navigation.goBack() }}
            ],
            { cancelable: true }
        )
    }

    // Activate set icon pop up
    _activeIconPopUp(){
        this.setState({ setIconPopUp: true })
    }
    // Deactivate set icon pop up
    _deactiveIconPopUp(){
        this.setState({ setIconPopUp: false })
    }

    _setIconPopUp(){
        if(this.state.setIconPopUp){
            return(
                <View style={ modal.iconsPopUp }>
                    <View style={ modal.chooseIconContent }>
                        <View style={ modal.chooseIconTitleWrapper }>
                            <Text style={ modal.chooseIconTitleText }>Choose your icon</Text>
                        </View>
                        <View style={ modal.iconsWrapper }>
                            {
                                iconTypes.map((item, index) => {
                                    return(
                                        <TouchableOpacity
                                            key={ index }
                                            onPress={() => { this._update(fields.newPlatformIcon, item)}}
                                            >
                                            <View>
                                                <Icon 
                                                    name={`logo-${item}`}
                                                    style={ modal.icon }
                                                    size={ 40 }
                                                />
                                            </View>
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </View>
                        <View style={ modal.cancelWrapper }>
                            <GenericButton 
                                onPress={() => { this._deactiveIconPopUp() }}
                                text={'Cancel'}
                            />
                        </View>
                    </View>
                </View>
            )
        }
    }
    
    render(){
        return(
            <SafeAreaView style={ styles.wrapper }>
                <ScrollView style={ styles.scrollView }>
                    <View style={ styles.field }>
                        <View style={ styles.labelWrapper }>
                            <View style={ styles.labelBorder }>
                                <Text style={ styles.label }>Title</Text>
                            </View>
                        </View>
                        <View style={ styles.inputWrapper }>
                            <TextInput 
                                autoCapitalize={'none'}
                                numberOfLines={1}
                                placeholder={ "Title" }
                                maxLength={ 60 }
                                style={ styles.input }
                                value={ this.state.title}
                                onChangeText={(value) => { this._update(fields.title, value )}}
                            />
                        </View>
                    </View>
                    <View style={ styles.field }>
                        <View style={ styles.labelWrapper }>
                            <View style={ styles.labelBorder }>
                                <Text style={ styles.label }>Name</Text>
                            </View>
                        </View>
                        <View style={ styles.inputWrapper }>
                            <TextInput 
                                autoCapitalize={'none'}
                                numberOfLines={1}
                                placeholder={ "Name" }
                                maxLength={ 60 }
                                style={ styles.input }
                                value={ this.state.name}
                                onChangeText={(value) => { this._update(fields.name, value )}}
                            />
                        </View>
                    </View>

                    <View style={ styles.field }>
                        <View style={ styles.labelWrapper }>
                            <View style={ styles.labelBorder }>
                                <Text style={ styles.label }>About me</Text>
                            </View>
                        </View>
                        <View style={ styles.inputWrapper }>
                            <TextInput 
                                autoCapitalize={'none'}
                                numberOfLines={5}
                                placeholder={ "About me..." }
                                multiline={ true }
                                maxLength={ 140 }
                                style={[ styles.input, styles.aboutMe ]}
                                value={ this.state.aboutMe}
                                onChangeText={(value) => { this._update(fields.aboutMe, value )}}
                            />
                        </View>
                    </View>


                    <View style={ styles.field }>
                        <View style={ styles.labelWrapper }>
                            <View style={ styles.labelBorder }>
                                <Text style={ styles.label }>Interested in</Text>
                            </View>
                        </View>
                        <View style={ styles.inputWrapper }>
                            <TextInput 
                                autoCapitalize={'none'}
                                numberOfLines={3}
                                placeholder={ "Interests" }
                                maxLength={ 140 }
                                style={ styles.input }
                                value={ this.state.interestedIn}
                                onChangeText={(value) => { this._update(fields.interestedIn, value )}}
                            />
                        </View>
                    </View>

                    <View style={ styles.field }>
                        <View style={ styles.labelWrapper }>
                            <View style={ styles.labelBorder }>
                                <Text style={ styles.label }>Links</Text>
                            </View>
                        </View>
                        <View style={[ styles.platformsWrapper, { paddingVertical: 10, } ]}>
                            { this._renderLinksList() }
                        </View>
                        <View style={ styles.optionBtnsWrapper }>
                            <View style={ modal.addBtnWrapper }>
                                <GenericButton 
                                    onPress={() => { this._openModal() }}
                                    text={'Add link'}
                                />
                            </View>


                            <View style={ modal.addBtnWrapper }>
                                <GenericButton 
                                    onPress={() => { this._updateProfileConfirm() }}
                                    text={'Update Profile'}
                                />
                            </View>

                            <View style={[ modal.addBtnWrapper, { marginBottom: 50,} ]}>
                                <GenericButton 
                                    onPress={() => { this._confirm('Are you sure?', "If you leave before updating, the changes you have made will not be saved.") }}
                                    text={'Cancel'}
                                />
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <Modal
                    visible={ this.state.modalVisibility }
                    animationType='slide'
                    style={ modal.wrapper }
                    >
                    <View style={ modal.content}>
                        <TouchableOpacity
                            onPress={()=>{ this._closeModal() }}
                            style={ modal.closeBtn}
                            >
                            <View>
                                <Icon 
                                    color={ Colors.stylish.tomato }
                                    size={40}
                                    name={ 'ios-close-circle-outline'}
                                />
                            </View>
                        </TouchableOpacity>
                        <View style={ modal.titleWrapper }>
                            <Text style={ modal.title }>Add new platform</Text>
                        </View>
                        <View style={[ styles.field, { marginTop: 30,} ]}>
                            <View style={ styles.labelWrapper }>
                                <View style={ styles.labelBorder }>
                                    <Text style={ styles.label }>Platform name</Text>
                                </View>
                            </View>
                            <View style={ styles.inputWrapper }>
                                <TextInput 
                                    numberOfLines={1}
                                    placeholder={ "Platform name" }
                                    maxLength={ 60 }
                                    style={ styles.input }
                                    value={ this.state.newPlatformName}
                                    onChangeText={(value) => { this._update(fields.newPlatformName, value )}}
                                />
                            </View>
                        </View>

                        <View style={ styles.field }>
                            <View style={ styles.labelWrapper }>
                                <View style={ styles.labelBorder }>
                                    <Text style={ styles.label }>Hyperlink</Text>
                                </View>
                            </View>
                            <View style={ styles.inputWrapper }>
                                <TextInput 
                                    numberOfLines={1}
                                    placeholder={ "Hyperlink" }
                                    maxLength={ 60 }
                                    style={ styles.input }
                                    value={ this.state.newPlatformNameLink}
                                    onChangeText={(value) => { this._update(fields.newPlatformNameLink, value )}}
                                />
                            </View>
                        </View>
                        <View style={[ styles.field, { flexDirection: 'row', alignItems: 'center'} ]}>
                            <Text style={ modal.selectIconLabel }> Select platform Icon </Text>
                            <TouchableOpacity
                                style={ modal.selectIconBtn }
                                onPress={ () => { this._activeIconPopUp() }}
                                >
                                <View>
                                    <Icon 
                                        size={30}
                                        color={ this.state.newPlatformIcon ? Colors.general.dark : Colors.stylish.tomato }
                                        name={ this.state.newPlatformIcon ? `logo-${this.state.newPlatformIcon}` : 'ios-image'}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={ modal.addBtnWrapper }>
                            <GenericButton 
                                onPress={() => { this._addLink() }}
                                text={'Add'}
                            />
                        </View>
                    </View>
                    { this._setIconPopUp() }
                </Modal>
            </SafeAreaView>
        );
    }
}

const mapPropsToState = (state) => ({
    session: state.Session
})

export default connect(mapPropsToState)(Index);



const fields = {
    title: 'title',
    name: 'name',
    aboutMe: 'aboutMe',
    interestedIn: 'interestedIn',
    newPlatformName: 'newPlatformName',
    newPlatformNameLink: 'newPlatformNameLink',
    newPlatformIcon: 'newPlatformIcon'
}