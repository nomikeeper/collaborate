import { combineReducers } from 'redux';
import { sessionReducer } from "redux-react-native-session";

// ------- Importing reducers
import MeReducer from "./Me";
import ActivityIndicatorReducer from "./ActivityIndicator";

export default combineReducers({
    Me: MeReducer,
    Session: sessionReducer,
    ActivityIndicator: ActivityIndicatorReducer,
})